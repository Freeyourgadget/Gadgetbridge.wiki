---
gitea: none
include_toc: true
---

## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/faq/


IF YOU WANT TO EDIT THE WIKI, do so on [codeberg.org](https://codeberg.org/Freeyourgadget/Gadgetbridge/wiki) . The wiki on github.com is a read-only mirror, as is the git repo itself. If you do not have access rights, ask for it in [this issue](https://codeberg.org/Freeyourgadget/Gadgetbridge/issues/3260).


# Frequently Asked Questions

## Is Gadgetbridge a firmware for smart watches?

No, Gadgetbridge is an Android companion app for smart devices like bands,
watches but also ear buds, multimeters and other gadgets.


## Why does Gadgetbridge ask for so many permissions?

In order to provide data "on the wrist", Gadgetbridge must get the data first.
See [[Permissions-Explained]] for more details. To ensure that none of the
accessed data is ever forwarded anywhere, Gadgetbridge does not have (and will
not have) Android network permission, which means that Gadgetbridge has no
means of sending any data anywhere. This is one of the most critical safeguards
in the Gadgetbridge permission system.

## Can we have a weather app implemented directly into Gadgetbridge?

No, as this would require network permission, see [here](FAQ#user-content-why-does-gadgetbridge-ask-for-so-many-permissions).

## What is the best device for Gadgetbridge

This is very difficult to answer and it depends on your needs and style, for
example if you want the device for sports, for development, for notifications
and so on. Look at each devices' wiki page for more details. We would also
appreciate if you help to fill the wiki pages of devices with more useful info
after you have used the device for some time.

### Pebble

[[Pebble]] used to be the main driving force for features in Gadgetbridge and
it had a lot of functions, including writing own apps for the watch. The step
counting was not great, but notification and things like SMS replies from the
watch were nice.

### Amazfit/Mibands

Miband/Amazfit bands and watches have gained a lot of popularity and have good
support in Gadgetbridge. They have fairly accurate step and heart rate sensors.
Total sleep hours are measured and info is provided but sleep cycles
(light/deep sleep) are not accurately discovered, but the heart rate pattern
allows you to observe your sleep patterns very well. Support in Gadgetbridge
includes workout tracking and also workouts that require GPS.

The new Amazfit/Miband devices  now require you to first obtain a secret
key from the official app before you can pair them with Gadgetbridge.

[[Amazfit Bip]] watch and the [[Mi Band 3]] band were the last devices that did
not require obtaining of this secret paring code for pairing. 

[[Mi Band 5]] requires obtaining of the secret pairing key but the
communication between the band and the watch is not encrypted and thus this
device is nice for reverse engineering.

[[Mi Band 6]] is a really nice band with good font support, full
front-face screen display and great features. The communication is even more
locked down: to pair you must first get the secret key and it is not possible
to just observe the Bluetooth traffic for reverse engineering because the data
is encrypted.

[[Zepp-OS]] watches are fairly well supported, but like many other devices require
 an auth key by using the official app at least once. The list if supported features 
is similar to other Huami devices. Zepp OS supports writing custom apps and for a 
tinkerer, this can open a lot of possibilities, even though the app support for Zepp 
in Gadgetbridge is currently limited. Device support includes loyalty card support 
by [Catima app](https://catima.app/).

### Bangle.js

[[Bangle.js]], mainly in it's version 2 is quite popular and has good features,
ongoing development, own apps and is completely open, but it may not be as
polished as the commercial products. It allows own apps and sending of custom
data via Intents.

### PineTime

Same can be said about the
[[PineTime]] which is also an open source smart watch where firmware is a work
in progress with features being gradually added in.

### Fossil

[[Fossil Hybrid HR]] is another very popular watch with a lot of features,
possibility to create own apps, send custom data via Intents. Sports tracking
features are not bad but are not great, there is no sleep tracking (in
Gadgetbridge). It requires you to use it with the official app at least once
and also to get a secret key before you can pair it with Gadgetbridge.

## Other devices?

- Many are supported, help to add details into [wiki](https://codeberg.org/Freeyourgadget/Gadgetbridge/wiki/), if you need access ask [here](https://codeberg.org/Freeyourgadget/Gadgetbridge/issues/3260).
- MiBand 8? Not at the moment
- [[Garmin devices]]? Initial support is in, [help is wanted](Garmin-devices)
- Huawei/Honor? Long term work in progress, you can offer help via Matrix: [#gadgetbridge:matrix.org](https://matrix.to/#/%23gadgetbridge:matrix.org)


## Do you support watches with Asteroid OS?

Yes! Initial support is in the master, feel free to help improving it.


## When will a new release appear on F-droid?

Unfortunately we have no control on when a new release will appear on F-droid,
but there are a few steps you can perform to check by yourself and follow the
progress. See more details [here in article: When will a new release appear on
f-droid](When-will-a-new-release-appear-on-f-droid).

## Is there a way to get more frequent updates of Gadgetbridge? Like a Nightly release?

Yes, you can use our Nightly F-Droid repository. You need to install F-Droid,
add our [repository](https://freeyourgadget.codeberg.page/fdroid/repo?fingerprint=CD381ECCC465AB324E21BCC335895615E07E70EE11E9FD1DF3C020C5194F00B2), update
F-Droid and install Gadgetbridge Nightly.

This release is automatically build and released every night when new commits
have been done to our repository. The Nightly release comes in two build types
\- "normal" Nightly release and "No Pebble provider" Nightly release. The "No
Pebble provider" release was created due to the fact that if you have either
the official Gadgetbridge [F-Droid
release](https://f-droid.org/packages/nodomain.freeyourgadget.gadgetbridge/) or
the original Pebble app installed, you cannot install another app (for example
the Gadgetbridge Nightly release) which provides something called "Pebble
provider" (`com.getpebble.android.provider`). The "No Pebble provider" version
allows to be installed alongside existing Gadgetbridge or Pebble app. If you
are planning to use the Nightly release and want to migrate your data from the
official release, make sure to be careful when exporting and backing up your
data. See detailed explanation in [Data Export Import Merging
Processing](Data-Export-Import-Merging-Processing#backup-your-data).

## I am considering to switch to the Gadgetbridge (Nightly) release. Is it stable enough to use it instead of the stable release?

The Nightly build is a nightly build of the master branch. The master branch is
what most developers of Gadgetbridge use, except that they have to self-compile
it more often, while the Nightly is prepared automatically. So yes, it is and
has been pretty stable. Hopefully it will remain this way :)

## I have a smart gadget and would like to help

Check if your gadget already has an implementation for one of the similar type/vendor. You could try to use the [Unsupported device 
pairing](https://codeberg.org/Freeyourgadget/Gadgetbridge/wiki/Home#testing-an-unsupported-device) which allows you to bypass the filter that distinguishes devices and you can add any device to Gadgetbridge as if it was any of the already supported devices. Then, look at older [PRs](https://codeberg.org/Freeyourgadget/Gadgetbridge/pulls) and see if you can follow along some of the previous additions.

Look at how you can gather data from communication between the watch and the
Android phone on which you first must run an app which can already talk to the
watch.

For simple data collection without root see [collecting data with
Wireshark](BT-Protocol-Reverse-Engineering#getting-live-bt_snoop-data-with-wireshark).
The collected data can then be used by implementing a support in Gadgetbridge,
for that see the following articles:

- [[Support for a new Device]]
- [[BT Protocol Reverse Engineering]]
- [[New Device Tutorial]]

## Can you help me to build my own app?

No, sorry, we do not have capacity for that.
