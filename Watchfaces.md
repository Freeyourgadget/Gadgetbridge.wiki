## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/internals/specifics/amazfit-bip/#colors

Gadgetbridge supports watchfaces via it's FW installer - watchface files are uploaded the same way as is a software update.
  
Watchface files for Xiaomi/Amazfit devices can be found for example on [[https://amazfitwatchfaces.com/]]. 

Download preferred .bin file to your mobile device, then via file manager open it with FW installer. 

In the Amazfit Bip/Bip Lite, One custom watchface can be used. 
Amazfit Band 5/MB5 have slots for 3 custom watchfaces.

Custom watchface cannot be deleted from the watch/band with Gadgetbridge, but can be overwritten by another one. Also, the stock watchfaces always remain and can be chosen from at any time.


### Example

#### Open downloaded watchface file in a file manager:

<img src="watchface_install_open_with.jpg" alt="Open With" width="400"/>


#### Choose FW Installer:

<img src="watchface_install_choose_installer.jpg" alt="Choose FW Installer" width="400"/>


#### Install:

<img src="watchface_install_fw_installer.jpg" alt="FW Installer" width="400"/>

#### Amazfit Bip: Color Palette
Here is the color palette supported by the Bip (note that the Bip Lite and Bip S support a wider range of color)

IMPORTANT: Do NOT flash a Bip S watchface on a Bip, you risk bricking the device!
 
| Color Name    | HEX    | Preview                                                      |
|---------------|--------|--------------------------------------------------------------|
| Black         | 000000 | ![#000000](https://via.placeholder.com/150x32/000000?text=+) |
| Blue          | 0000FF | ![#0000FF](https://via.placeholder.com/150x32/0000FF?text=+) |
| Green         | 00FF00 | ![#0000FF](https://via.placeholder.com/150x32/00FF00?text=+) |
| Red           | FF0000 | ![#FF0000](https://via.placeholder.com/150x32/FF0000?text=+) |
| Cyan          | 00FFFF | ![#00FFFF](https://via.placeholder.com/150x32/00FFFF?text=+) |
| Magenta       | FF00FF | ![#FF00FF](https://via.placeholder.com/150x32/FF00FF?text=+) |
| Yellow        | FFFF00 | ![#FFFF00](https://via.placeholder.com/150x32/FFFF00?text=+) |
| White         | FFFFFF | ![#FFFF00](https://via.placeholder.com/150x32/FFFFFF?text=+) |
| *Transparent* | FEFE00 | ![#FEFE00](https://via.placeholder.com/150x32/FEFE00?text=+) |