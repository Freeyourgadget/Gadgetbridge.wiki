## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/wearables/xiaomi/

## Mi Band/Huami devices

The Mi Band line of watches are manufactured by Huami for Xiaomi. In some places, these devices still have the Huami name (Huami Amazfit Bip) etc.

Gadgetbridge has support for the following watches from the Huami/Mi Band family:

- [[Mi Band 1]]
- [[Mi Band 2]]
- [[Mi Band 3]]
- [[Mi Band 4]]
- [[Mi Band 5]]
- [[Mi Band 6]]
- [[Mi Band 7]]
- [[Amazfit Band 5]]
- [[Amazfit Band 7]]
- [[Amazfit Bip]]
- [[Amazfit Bip Lite]]
- [[Amazfit Bip S]]
- [[Amazfit Bip U]]
- [[Amazfit Cor]]
- [[Amazfit Cor 2]]
- [[Amazfit GTR]]
- [[Amazfit GTR 3]]
- [[Amazfit GTR 4]]
- [[Amazfit GTS]]
- [[Amazfit GTS 3]]
- [[Amazfit GTS 4]]
- [[Amazfit GTS 4 Mini]]
- [[Amazfit Neo]]
- [[Amazfit T-Rex]]
- [[Amazfit T-Rex 2]]

[[Amazfit-Bip]] is one of the best documented devices and in reality, many options described there might work for some other Huami gadgets.