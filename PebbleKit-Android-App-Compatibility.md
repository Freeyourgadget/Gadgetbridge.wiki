## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/basics/topics/pebblekit/

## Confirmed working Apps

* [Dashboard](https://play.google.com/store/apps/details?id=com.wordpress.ninedof.dashboard)
* [Meerun](https://play.google.com/store/apps/details?id=com.bwa.meerun)
* [Notification Center](https://play.google.com/store/apps/details?id=com.matejdro.pebblenotificationcenter&hl=en_US)
* [PebbleTasker](https://play.google.com/store/apps/details?id=com.kodek.pebbletasker&hl=en)
* [RunnerUp](https://f-droid.org/repository/browse/?fdfilter=runnerup&fdid=org.runnerup)
* [JayPS](https://android.izzysoft.de/repo/apk/com.njackson)

## Apps with issues / workarounds

### [Canvas for Pebble](https://play.google.com/store/apps/details?id=com.pennas.pebblecanvas)
Does not work. Complains about `Pebble not connected`. [#130](../issues/130) One can manually install the pbw from `/sdcard0/Android/data/com.pennas.pebblecanvas/files/`. Might work with Gadgetbridge > 0.7.2
Works. Version 4.12 of Android app (with Pebble app version 4.8). Tested with Gadgetbridge 0.26.5
### Pebble Dialer
Works on Pebble classic, might be broken or incomplete on Pebble Time, since it distinguishes between Aplite and Basalt (for caller pictures). Needs more testing.
### [Push to Pebble](https://play.google.com/store/apps/details?id=com.mohammadag.pushtopebble)
Does not Work. Complains about `No Pebble Connected or Pebble app not installed`. [#130](../issues/130).  Might work with Gadgetbridge > 0.7.2
### [Sleep as Android](https://play.google.com/store/apps/details?id=com.urbandroid.sleep)

Works, but needs to be set up with official Pebble app first:

1. Set up Pebble with Gadgetbridge normally
2. [Install Sleep as Android to Pebble](https://codeberg.org/Freeyourgadget/Gadgetbridge/wiki/Pebble-Watchfaces#where-can-i-find-watchfaces)
2. Install official Pebble app to your phone (only install, no need to open the app or set it up)
3. Open Sleep as Android on your phone
4. Go to Settings -> Wearables -> Use Wearables
5. Select Pebble
6. Start Sleep Tracking on the Phone. App should start on the Pebble. Make sure everything works correctly
7. Uninstall official Pebble app
