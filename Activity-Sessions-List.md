---
gitea: none
include_toc: true
---

## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/basics/features/activities/#activity-list

## Activity (Sessions) List

This tab in [charts](Activity-and-Sleep-Charts#activity-sessions) allows
observing and analyzing your movement/activity sessions throughout the day
without having to turn on explicit Activity Tracking on the watch.


### Activity List dashboard

Make sure to adjust your personal settings and goals, available in menu:
`Settings → About you` (or now also available via `Settings → About you` in the
Charts screens → Settings icon.

- `Step length` → this will be used to calculate distance from the number of
  Active steps.

The following items will be used to calculate percentage charts between your
achievement and your goal:

- `Daily target: distance`
- `Daily target: active time`
- `Daily step target`

### Activity sessions

You can change how activities are being detected, via Charts → `Settings →
Activity list`. Here are the determining factors:

- Activity session is detected by steps per minute or by activity intensity. Minumum steps per minute ([this is internally linked to minimum intensity](https://codeberg.org/Freeyourgadget/Gadgetbridge/src/branch/master/app/src/main/java/nodomain/freeyourgadget/gadgetbridge/activities/charts/StepAnalysis.java#L49)) required for an activity to be detected as activity session can be set in the settings `Minimal steps per minute to detect activity`
- Activity session must be at least X minutes long, this minimal duration can be set in the settings `Minimal activity length`
- Activity session ends with a pause longer then a given interval, it can be set in the settings `Pause length to split activities`
- Activity session is marked as Running if steps/m > 120, this can be set in the settings `Minimal steps per minute to detect run`
- Activity session is [marked as Excercise](https://codeberg.org/Freeyourgadget/Gadgetbridge/src/branch/master/app/src/main/java/nodomain/freeyourgadget/gadgetbridge/activities/charts/StepAnalysis.java#L147) if it has less the 200 steps but has heartRateAverage > 90 && intensity > 15

<img src="images/activity_list/activity_list_dashboard.png" alt="activity list dashboard" width="200">

<img src="images/activity_list/activity_list.png" alt="activity list" width="200">


## Icons meaning:

- <img src="images/activity_list/ic_total_steps.png" alt="icon steps total" width="20"> Total Steps - this is the number of your total steps today. It is shown to be able to see a difference between active and total steps.
- <img src="images/activity_list/ic_steps.png" alt="icon steps" width="20"> Active Steps
- <img src="images/activity_list/ic_duration.png" alt="icon duration" width="20"> Duration of activity
- <img src="images/activity_list/ic_distance.png" alt="icon distance" width="20"> Distance (Active steps * Step length)
- <img src="images/activity_list/ic_intensity.png" alt="icon intensity" width="20"> [Movement Intensity](#intensity-of-activity) (as reported by the band)
- <img src="images/activity_list/ic_heartrate.png" alt="icon heart rate" width="20"> Average Heart rate

## Live activity detection

When you perform activity synchronization and an ongoing activity is detected,
toast is displayed with current status. You can enable/disable this in charts
settings.

<img src="images/activity_list/ongoing_toast.jpg" alt="ongoing activity" width="200">

## Preference settings:

<img src="images/activity_list/activity_list_preferences.png" alt="activity list preferences" width="200">

## Intensity of activity

One of the new items in Activity List but also in the Sleep charts is
`intensity` <img src="images/activity_list/ic_intensity.png" alt="icon intensity" width="20">.
It is an interesting value which typically does not surface as a
number, but in fact it has been used throughout Gadgetbridge charts a lot: the
green spikes on axis Y in activity charts? Intensity. The light and dark blue
spikes on axis Y in your sleep charts? Also intensity. It is a value (normalized
to decimal between 0 and 1) that is representing a strength or an intensity of
your activity, as determined by the bracelet. At night, the more you move, the
higher this intensity is. During an activity or walk, the more your arm moves,
the number seems to go up. So it seems good to expose this number even more, to
be able to observe it, typically labeling it as `Movement intensity`. The
displayed result is not calculated as an average but rather as a total sum of
the intensity of a particular activity in activity list or in your sleep.
Hopefully you'll find it useful.

<a href="raw/images/movement_intensity.png"><img src="images/movement_intensity.png" width="200"></a>

### Further possible improvements:

- detect activities by ActivityKind (if stored in db)
- better detection of activity sessions - right now, there is one generic threshold. Maybe we could try to detect more "levels of activity" and split activities by these levels.
- add nicer names to some activity sessions (e.g. Morning walk)
- burned calories
- ... :)

