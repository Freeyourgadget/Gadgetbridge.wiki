## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/internals/specifics/amazfit-bip-s/

## CAUTION
This feature **has the potential to brick your Amazfit Bip S. You are doing this at your own risk**. That being said, it did not happen to us yet.

## Getting the firmware
Since we may not distribute the firmware, you have to find it elsewhere :(
Amazfit Bip S. Early firmware updates for prototypes were part of Mi Fit, but are only made available via OTA updates.

## Choosing the right firmware version for your Model

TODO

## Installing the firmware

TODO


## Known Firmware Versions (Bip S without NFC)

fw ver   | tested | notes                   | res ver | gps ver           |fw-md5 | res-md5 | gps-md5 |
---------|--------|-------------------------|---------|-------------------|-------|---------|---------|
2.0.0.80 | yes    | pre-installed firmware  | ?       | 18344,eb2f43f,126 |   ?    | ?       | ? |
2.1.1.08 | WIP    | needs a new font format | 93      | 19226,f3a8ad3,135 |   ?    | ?     | 2c95f2a6079bbef04ab47c1ffbe5b5fd |


### Notes on Fonts
- The initial 2.0 firmware was compatible with original Bip font files, also the emoji font. All languages seemed to be supported (eg. German, Russian and Chinese). 
- With 2.1, the fonts are completely incompatible, it seems that the selectable languages depend on which font is installed. The one we installed took away chinese support and the fonts looks slim (like a Bip latin firmware font). Notably Japansese character AND German umlauts work when English is selected. There is no longer a need for merging fonts but custom emoji fonts for the Bip no longer work.