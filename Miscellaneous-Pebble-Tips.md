### OsmAnd navigation messages
[<img src="https://i.imgur.com/Htov7I2.png" width="200" align="right">](https://i.imgur.com/Htov7I2.png) When using OsmAnd with your Pebble and Gadgetbridge, you might feel spammed by navigation hints and your Pebble almost constantly vibrating whenever the distance to your next waypoint gets updated. To get rid of that noise without switching off the otherwise useful hints altogether, first understand what is causing the issue. OSMAnd itself sends the notifications on two channels:

* standard Android notifications: these are causing the noise
* PebbleKit: these are the ones one most likely wants to keep

So the trick is easy if you know it: In Gadgetbridge settings, go to *Pebble Settings,* then *Notification Blacklist* – and simply blacklist Android notifications for OSMAnd, while keeping PebbleKit notifications enabled for for OsmAnd (see screenshot). Issue solved.