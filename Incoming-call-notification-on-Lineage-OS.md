## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/basics/features/notifications/

Moved to [Notifications](Notifications#incoming-call-notification-on-lineage-os) article.