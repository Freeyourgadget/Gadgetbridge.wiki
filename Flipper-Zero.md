## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/others/flipper/

# Flipper Zero
A Multi-tool Device for Geeks 

## Support in Gadgetbridge

It's main purpose currently is to provide an Intent-based API to Tasker and similar apps to play sub-GHz files.

In the future, file management and other features might be useful.


## Using Intent based API to play a file

### Check if Flipper Zero is connected:

```bash
     adb shell am broadcast \
    -a "nodomain.freeyourgadget.gadgetbridge.BLUETOOTH_CONNECT" \
    -e "EXTRA_DEVICE_ADDRESS" "xx:xx:xx:xx:xx"
```
Gadgetbridge then checks if the device is already connected. If it isn't, it tries to establish a connection. Whenever the device is initialized, Gb responds with the intent `nodomain.freeyourgadget.gadgetbridge.BLUETOOTH_CONNECTED`, and by reading the extra `EXTRA_DEVICE_ADDRESS` I can determine whether it's the Flipper that recently connected.

### If so, I send the following Intent to play the file:

```bash
	adb shell am broadcast \
	-a "nodomain.freeyourgadget.gadgetbridge.flipper.zero.PLAY_FILE" \
	-e "EXTRA_FILE_PATH" "/ext/subghz/Vehicles/Tesla/charge_650_long.sub" \
	-e "EXTRA_DURATION" "5000"
```
### Optional parameters

Optionally, you can specify EXTRA_APP_NAME` with `infrared`, `NFC` or any other app.
In case of `infrared`, you also need to add `EXTRA_BUTTON_NAME` with the value of a button of a remote.

I then listen for the Intent `nodomain.freeyourgadget.gadgetbridge.flipper.zero.PLAY_DONE` from Gb to react to the file play finishing.