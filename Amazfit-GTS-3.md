## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/wearables/amazfit/#gts-3

See the [Zepp OS](Zepp-OS) page for a list of features and issues common to Zepp OS devices.

### Tested Firmwares

- 7.42.5.1

### Tested Hardware

- 0.76.17.4
