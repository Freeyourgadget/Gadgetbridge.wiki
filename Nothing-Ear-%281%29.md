## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/headphones/nothing/

Supports:

- find device
- read battery level
- set to pause/play based on in-ear detection
- set Audio profiles:
    - ANC (Automatic Noise Canceling) 
    - ANC-Lite
    - Transparency (Ambient sounds)
    - Off (No canceling, no ambient))