---
gitea: none
include_toc: true
---

## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/basics/pairing/huami-xiaomi-server/


### Server based pairing

New devices and older devices with updated firmware require server based
pairing. This means, that you **absolutely must** use the original
MiFit/Amazfit/Zepp app to make the initial pairing, retrieve the pairing key
and then use this key to pair with Gadgetbridge. Currently, **Mi Band 4**, **Mi
Band 5**, **Mi Band 6**, **Mi Band 7**, **Amazfit Bip Lite**, **Amazfit GTR** (and more as time goes on) are known to
be the first devices with this enabled from the beginning.



### tldr; Entering the pairing key

1. pair the band in the official app
2. obtain the key
3. kill or uninstall the MiFit/Amazfit/Zepp app
4. unpair the band from Android pairing
5. after scanning, long press the device in the Discovery screen
6. enter the key to the `Auth Key`

<img src="images/pairing/pairing-huami.jpg" alt="pairing huami" width="350"/>

<img src="images/pairing/auth-key.jpg" alt="auth key" width="350"/>


### Requirements

To set up your band/watch with Gadgetbridge, you need to get the unique key for your band. It is stored on Huami servers or in original Mifit/Zepp apps database. There are several ways to obtain the key.

### Obtaining the unique key

- The key is available either from Mifit/Zepp internal data storage or from
  their online servers. You **must not unpair** the watch from their original
  app as this would erase the key.

#### On rooted phones

On **rooted** phone you may grab the key from MiFit/Amazfit database which
means that you must:

- install Zepp Life (for Amazfit GTS install Zepp)
- create an account
- pair the band/watch
 
Then, execute the following command in a root shell terminal:

##### for Mi Band 4/5/6, Amazfit Bip Lite and Amazfit GTR
```
sqlite3 /data/data/com.xiaomi.hm.health/databases/origin_db_[YOURDBNAMEHERE] "select AUTHKEY from DEVICE"
```
##### for Amazfit GTS, Amazfit T-Rex and Amazfit T-Rex Pro
```
sqlite3 /data/data/com.huami.watch.hmwatchmanager/databases/origin_db_[YOURDBNAMEHERE] "select AUTHKEY from DEVICE"
```
##### for Mi Band 6 with Mi Fitness (Xiaomi Wear) (com.xiaomi.wearable)
```
sqlite3 /data/data/com.xiaomi.wearable/databases/device_db "select detail from device"
```
Within the contained JSON, the attribute `auth_key` contains the key.

**NOTICE:** Every time you hard reset the band/watch, the Bluetooth MAC Address will be changed and you must [grab a new key](#obtaining-unique-key)! Also, anytime you unpair your band/watch from MiFit/Amazfit/Zepp the pairing key will be invalidated and you must make new pairing in MiFit/Amazfit/Zepp app.

**IMPORTANT**: Enter your key prefixed with `0x` ( eg. if your key is `fedcba01234567890fedcba012345678` enter `0xfedcba01234567890fedcba012345678`). Do not press the Enter key. Make sure `0x` is really zero (`0`) and lowercase X (`x`) since some keyboards might use different symbols.

#### On non-rooted phones

##### Using a python script with your account details

https://codeberg.org/argrento/huami-token

Please see the [README](https://codeberg.org/argrento/huami-token/src/branch/master/README.md) for a detailed description on the procedure.

If using a watch/band from Xiaomi, this method [may not work at the moment](https://codeberg.org/argrento/huami-token/issues/66).

##### Using the huafetcher app with your account details

https://codeberg.org/vanous/huafetcher
Please see the [README](https://codeberg.org/vanous/huafetcher/src/branch/master/README.md) for a detailed desctiption on the procedure

This allows you to run the huami-token script right on your phone, so that you
can easily copy and paste the key. If using a watch/band from Xiaomi, this method [may not work at the moment](https://codeberg.org/argrento/huami-token/issues/66).

##### From Mi Fitness logs

If your device is supported by the Mi Fitness app, there are reports that it prints the authKey in the logs (#2991), which are available in the phone's internal storage. Accessing the logs does _not_ require root and the path to the relevant file is `/sdcard/Android/data/com.xiaomi.wearable/files/log/XiaomiFit.device.log`.

The key can be printed with the following two commands, assuming you've enabled USB debugging on your mobile device

```bash
adb shell
grep -E "authKey=[a-z0-9]*," /sdcard/Android/data/com.xiaomi.wearable/files/log/XiaomiFit.device.log | awk -F ", " '{print $17}' | grep authKey | tail -1 | awk -F "=" '{print $2}'
```

It seems that in the latest versions of Mi Fitness (+3.20.2i) the field `authKey` is null so you have two more options with the fields `token` or `encryptKey`

```bash
adb shell
grep -E "encryptKey=[a-z0-9]*," /sdcard/Android/data/com.xiaomi.wearable/files/log/XiaomiFit.device.log | awk -F ", " '{print $12}' | grep encryptKey | tail -1 | awk -F "=" '{print $2}'
```

or


```bash
adb shell
grep -E "token=[a-z0-9]*," /sdcard/Android/data/com.xiaomi.wearable/files/log/XiaomiFit.device.log | awk -F ", " '{print $11}' | grep token | tail -1 | awk -F "=" '{print $2}'
```

This process can be done within [Termux](https://termux.dev/en/) in the same device if you can access to `/sdcard/Android/data/com.xiaomi.wearable/files/log/XiaomiFit.device.log`. It seems that in custom ROMs is possible but not in some stock ROMs like in the Pixel's. If you are able to access it, you can copy the log to a more comfortable location and run a command like the followings:

```bash
adb shell
grep -E "encryptKey=[a-z0-9]*," ~/downloads/XiaomiFit.device.log | awk -F ", " '{print $12}' | grep encryptKey | tail -1 | awk -F "=" '{print $2}'
```

or

```bash
grep -E "token=[a-z0-9]*," ~/downloads/XiaomiFit.device.log | awk -F ", " '{print $11}' | grep token | tail -1 | awk -F "=" '{print $2}'
```

##### freemyband

On a non rooted phone you may consider using https://www.freemyband.com/ **NOTE
that the procedure described there is not encouraged or supported by the
Gadgetbridge developers and the devs have no information on that app and
whether it is safe to use**


### Preparations for adding to Gadgetbridge

- do not unpair the band/watch from MiFit/Amazfit/Zepp app
- kill or uninstall the MiFit/Amazfit/Zepp app
- ensure GPS/location services are enabled
- unpair the band/watch from your *phone's bluetooth* (not from the app), as per below

You are allowing "location services", which include GPS, because on Android,
Bluetooth discovery is considered to be able to [reveal a
location](https://stackoverflow.com/questions/33045581/location-needs-to-be-enabled-for-bluetooth-low-energy-scanning-on-android-6-0).
You can disable "location services" access for Gadgetbridge after pairing your
device.

If you already connected your band/watch with your phone, remove it from
Android Bluetooth settings. The procedure depends on which ROM is installed on
your smartphone. Here is an example for LineageOS 16 (Android 9):

[![connected](https://i.imgur.com/nvk6wNLm.png)](https://i.imgur.com/nvk6wNL.png) [![forget device](https://i.imgur.com/KEmFoLrm.png)](https://i.imgur.com/KEmFoLr.png) [![confirm](https://i.imgur.com/TutygHam.png)](https://i.imgur.com/TutygHa.png) [![cleared](https://i.imgur.com/Seszcmcm.png)](https://i.imgur.com/Seszcmc.png)

### Adding to Gadgetbridge

#### 1. Once you own your unique key for your band/watch and [preparations](#preparations) are done, start Gadgetbridge, go to the home screen and tap "(+)":

[![main menu](https://i.imgur.com/Q3sSXe7m.png)](https://i.imgur.com/Q3sSXe7.png)

#### 2. Allow access to location services (required since Android 6) and Bluetooth. Start discovery:

You are allowing "location services", which include GPS, because on Android,
Bluetooth discovery is considered to be able to [reveal a
location](https://stackoverflow.com/questions/33045581/location-needs-to-be-enabled-for-bluetooth-low-energy-scanning-on-android-6-0).
You can disable "location services" access for Gadgetbridge after pairing your
device.

[![allow location services](https://i.imgur.com/ukl6uEnm.png)](https://i.imgur.com/ukl6uEn.png) [![allow Bluetooth](https://i.imgur.com/HyC8S1Zm.png)](https://i.imgur.com/HyC8S1Z.png) [![start discovery](https://i.imgur.com/9VeoF33m.png)](https://i.imgur.com/9VeoF33.png)

#### 3. After a while, your band/watch should appear. Long press on "*Device name*", for example "*Mi Smart Band 4*":

[![found device](https://i.imgur.com/73Wgm0Jm.png)](https://i.imgur.com/73Wgm0J.png)

#### 4. Scroll down, tap "Auth Key" and enter your own key. Do NOT hit Enter/Return key.

**IMPORTANT: Enter your key prefixed with `0x`** ( eg. if your key is
`fedcba01234567890fedcba012345678` enter `0xfedcba01234567890fedcba012345678`).
Do not press the Enter key. Make sure `0x` is really zero (`0`) then lowercase X (`x`) since some keyboards might
use different symbols.

[![enter key](https://i.imgur.com/QEdTTbrm.png)](https://i.imgur.com/QEdTTbr.png)

#### 5. Go back and tap on "*Device name*", for example "*Mi Smart Band 4*". Pairing should start now:

[![found device](https://i.imgur.com/73Wgm0Jm.png)](https://i.imgur.com/73Wgm0J.png) [![pairing](https://i.imgur.com/E2svdjTm.png)](https://i.imgur.com/E2svdjT.png)

#### 6. After successful connection you will see your band/watch on the home screen.

[![successful](https://i.imgur.com/bWiKpdKm.png)](https://i.imgur.com/bWiKpdK.png)

Congratulations, you did it!

-----

### Troubleshooting

If you encounter problems with Bluetooth discovery or pairing process, try the
following steps:

* Make sure to have first paired in MiFit/Amazfit/Zepp app
* Make sure MiFit/Amazfit/Zepp is not running
* Make sure you did not unpair your band/watch from MiFit/Amazfit/Zepp
* Make sure to add `0x` in from your key when pasting/typing it to Gadgetbridge
* Make sure `0x` is really zero (`0`) then lowercase X (`x`) since some keyboards might use different symbols; [see here](https://codeberg.org/Freeyourgadget/Gadgetbridge/issues/1775#issuecomment-47662).
* Do not press Enter/Return key when typing the key
* Make sure that band/watch is unpaired from the phone's Bluetooth
* Toggle Bluetooth OFF and ON
* Reboot your smartphone 
* Reboot your band/watch (More-> Settings-> Reboot)

<!-- removed pairing tip, at least for now

* Let Android find the Mi Band 4 for you. To do it meet the [preparations](#preparations), then start Gadgetbridge and go to the discovery menu:

[![discovery menu](https://i.imgur.com/wzYeAVKm.png)](https://i.imgur.com/wzYeAVK.png)

Switch then to android Bluetooth settings and tap "Pair new device". Now Android will try to find your Mi Band 4:

[![Android bt settings](https://i.imgur.com/XtAVjnQm.png)](https://i.imgur.com/XtAVjnQ.png) [![pair new device](https://i.imgur.com/8DSbVC6m.png)](https://i.imgur.com/8DSbVC6.png)

...wait now.... it may take some (long) time... wait please...*(Bluetooth is sometimes weird)*...

as soon as the device appears:

[![Android found device](https://i.imgur.com/tIrKfHem.png)](https://i.imgur.com/tIrKfHe.png)

switch back to the discovery menu in Gadgedbridge. Now you will see the Mi Band 4. Continue as described under [Setup, step 3](#3-after-a-while-your-mi-band-4-should-appear-long-press-on-mi-smart-band-4). Good luck!

-->
