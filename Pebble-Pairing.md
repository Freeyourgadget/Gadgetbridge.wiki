## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/basics/pairing/pebble/

Pebble pairing can be very frustrating and there are many issues in our issue tracker about it. Recommendations vary from "You should NOT pair a BTLE device via Android's Bluetooth settings" to "Connect Pebble through Android Settings"... ¯\\_(ツ)_/¯


### Pairing a Pebble2

Instructions copied from [#2118](https://codeberg.org/Freeyourgadget/Gadgetbridge/issues/2118#issuecomment-237690) but reduced to the necessary steps.

Make sure Pebble related setting in Gadgetbridge are set as defaults, if you are not sure, backup your data and delete all data from Gadgetbridge to ensure clean settings. Make sure to have "ignore bonded devices" disabled. [CDM](https://codeberg.org/Freeyourgadget/Gadgetbridge/wiki/Companion-Device-pairing) can be enabled for additional functions.

* Connect Pebble through Android Settings
* Turn bluetooth on the pebble off -> remove the phone from the pebble -> turn bluetooth on again
* Open Gadgetbridge and press the + button to add a device
* Click on the pebble (marked as already connected)
* There should be a toast on the phone to confirm, followed by the pair screen on the pebble and after confirming the pebble should be visible in Gadgetbridge


### Pairing a Pebble BTLE

Instructions quoted from https://codeberg.org/Freeyourgadget/Gadgetbridge/issues/1098#issuecomment-41052

You should **NOT** pair a BTLE device via Android's Bluetooth settings. If you already did so and ran into problems with it in Gadgetbridge (which you certainly will if you did), first step is to "forget the device" – that is the Pebble in Android's Bluetooth settings as well as the Android device in the Pebble's settings.

* set/keep the client only option enabled
* forget the Pebble (on the android device) and the android device (on the pebble)
* toggle bluetooth on both pebble and android
* add the pebble again (pressing the **+** button in Gadgetbridge)


### Pairing a Pebble OG

* disable Companion Pairing (experienced on Android 8)
* pair via Gadgetbridge's `Add device + button`
* do not select the LE Pebble but the "normal" Pebble in the list of found devices




