## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/wearables/xiaomi/#mi-band-3

### Contents
* [Gadgetbridge Configuration](Configuration#mi-band--amazfit-specific-settings)
* [[Mi Band 3 Firmware Update]]
* [Alarms](Huami-Alarms)
* [Weather information](Weather)
* [[Starting Activity from the band]]
* [Heartrate measurement](Huami-Heartrate-measurement)
* [Heart Rate 3rd party apps data sharing](Heart-Rate-data-sharing)
* [Sleep Detection](Huami-Deep-Sleep-Detection)

