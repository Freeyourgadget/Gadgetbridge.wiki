## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/wearables/amazfit/#gtr-3-pro

See the [Zepp OS](Zepp-OS) page for a list of features and issues common to Zepp OS devices.

### Tested Firmwares

- 8.45.7.1

### Tested Hardware

- 0.83.19.2
