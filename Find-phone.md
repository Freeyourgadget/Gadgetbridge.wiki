## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/basics/features/find-phone/

## Find phone

In order for the Find phone feature to work correctly on Android 10 and up, Android requires that the pairing between the mobile device (phone) and the smart bracelet/watch is done via Companion Device Management, read about it [here](Companion-Device-pairing) for more details.

## MIUI and other custom ROMs

On some custom ROMs such as MIUI, it may be necessary to manually grant some permissions:

- Show on lockscreen
- Open new windows on background

See #3309 for more details.

## Adjusting the ringing tone

By default, the Incoming call ringing tone will be used when the Find phone feature is used but you can customize this via Gadgetbridge menu - Notification settings - Ping tone.

