## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/basics/topics/zeppos

# Zepp OS

* [Huami Server Pairing](Huami-Server-Pairing)
* [Gadgetbridge Configuration](Configuration#mi-band--amazfit-specific-settings)
* [aGPS data for GPS chip](HUAMI-GPS)
* [Weather information](Weather)
* [Alarms](Huami-Alarms)
* [Custom watchface](Watchfaces)
* [Heart Rate 3rd party apps data sharing](Huami-Heartrate-measurement)

This page lists the current known issues and missing features for Zepp OS devices. This includes:

- [[Amazfit Balance]]
- [[Amazfit Band 7]]
- [[Amazfit Bip 5]]
- [[Amazfit GTR 3]]
- [[Amazfit GTR 3 Pro]]
- [[Amazfit GTR 4]]
- [[Amazfit GTR Mini]]
- [[Amazfit GTS 3]]
- [[Amazfit GTS 4]]
- [[Amazfit GTS 4 Mini]]
- [[Amazfit T-Rex 2]]
- [[Amazfit T-Rex Ultra]]
- [[Mi Band 7]] (Xiaomi Smart Band 7)

The following Zepp OS devices should also be supported as of #3258, but are currently marked as **experimental** / not tested:

- [[Amazfit Active]]
- [[Amazfit Active Edge]]
- [[Amazfit Falcon]]
- [[Amazfit Cheetah]]

These should work, but were not tested at all. If you confirm any of these devices works or face any issues, please open an issue or reach out in Matrix.

The Mi Band 8 is **not** a Zepp OS device, and is not compatible (#3146).

### Implemented features

The following list includes all implemented features for Zepp OS devices. The actual supported features can vary for each device.

- Set time
- World Clocks
- Activity sync (activity, sleep, workout)
- Canned Messages (Calls, SMS, Notifications). Request from watch on connection.
- Alarms - Set / Request / Notification on change from watch
- Calendar (create, delete, request events, set location)
- Vibration Patterns (Set only, no request from watch)
- User Info
- Camera remote
- Notifications
  - Send to watch
  - Delete notifications on phone/watch when deleted on the other
  - Calls
  - Answer phone calls on watch
  - Custom notification icons
  - Reply to notifications from watch
- Workout
  - Start/stop on phone when it starts/stops on watch
  - Send GPS to watch (for watches without GPS)
- Reminders (set, request from watch)
- Music
  - Music Info
  - Volume
  - Music buttons
- Device actions (fall asleep, wake up)
- Real-time heart rate
- Real-time steps
- Find Phone from watch
  - Stop on watch when stopped on phone
  - Toggle between ring / vibrate
- Battery Info
- HTTP requests (we handle them selectively - for now only weather)
- Set and request all configs. Non-extensive list:
  - Display Items / Shortcuts
  - Heart Rate
    - High-accuracy sleep tracking
    - Sleep breathing quality
    - All-day HR measurement, with high and low alerts
    - Stress monitoring, relaxation reminders
    - All-day SPO2 monitoring / low SPO2 alert
  - Fitness goal / goal notification
  - Password
  - Language / react to change on band
  - Screen brightness / timeout
  - Screen On on notifications
  - Distance / temperature unit
  - Inactivity warnings (enabled, schedule, dnd, dnd schedule)
  - Lift wrist (mode, schedule, sensitivity)
  - Always on Display (mode, schedule)
  - Night mode (mode, schedule)
  - Do Not Disturb (mode, schedule)
  - BT connection advertisement
  - 3rd party realtime HR access
  - Activity monitoring
  - Auto-brightness
  - Always-on Display Style
  - Sound and Vibration
    - Alert tone
    - Cover to Mute
    - Volume
    - Vibrate for alerts
    - Crown vibration
    - Text to Speech
  - Workout detection (category, alert, sensitivity)
  - Keep screen on during a workout
  - GPS
    - Presets, bands, combination, satellite search
    - AGPS expiry reminder
    - AGPS updates
  - Sleep mode (sleep screen, smart enable)
  - Long press upper button
  - Short press lower button
  - Control center display items (swipe from top of screen)
  - Wearing direction
  - Offline Voice
- Install watchfaces
- Install firmware upgrades (full and partial)
- Install applications
- Upload GPX routes
- Setting watchface from phone
- Morning Updates
- Shortcut cards (resident application settings)
- Contact management
- Taking screenshots (not supported on Mi Band 7)
- Stress Sync and charts
- PAI Sync and charts
- Membership / loyalty cards with [Catima](https://catima.app/)
- Logs from watch apps
- Weather (daily, hourly)
- Sun & Moon

### Zepp OS 3 issues

Zepp OS 3 is the latest version of Zepp OS, and still has some issues:

- File transfer is not working. This includes:
  - Notification icons
  - Gpx uploads
  - AGPS uploads
- Watchface upload is not working (#3445)

### Known Issues and limitations

- 3rd party realtime HR access is not working, even with the official app
- Weather is missing some data, due to GB limitations:
  - Only works for a single location
  - No tide support
- Notification icons require high MTU (enable in device settings)
- Updating reminders database when requesting from band is not implemented
- Sleep data is sometimes off

### Missing Features

- App integrations
- Calendar: repeating events, event notification delay
- Heartrate manual measurement from phone
- Music on device memory (needs Wi-Fi / Network Addon)
- Silent Mode - WIP on #2698
- Configure date and time format from phone
- Application Management (experimental)
    - Known issues: allows uninstall system apps (**dangerous**), does not always refresh the UI
- Some configuration options on phone:
    - Wake Up Alarm
    - Sleep Mode
    - Bedtime Reminders

#### Missing data syncs

- Sleep SPO2
- Body temperature (protocol implemented, no UI)
- SPO2 Sync (experimental feature, no UI)
- Resting HR (experimental feature, no UI)
- Max HR (experimental feature, no UI)
- Manual HR measurements (experimental feature, no UI)
- Speel respiratory rate (experimental feature, no UI)

#### Missing integrations with built-in apps

- Alexa (experimental feature)
- Female Health / Cycle Tracking
- To-Do List
- Voice memos (needs Wi-Fi / Network Addon)
- Sleep schedule

### Experimental Features

**WARNING: Experimental features may be unstable. Proceed at your own risk**

In order to enable experimental features, you can use the [[Intent-API]]:

```bash
adb shell am broadcast \
    -a "nodomain.freeyourgadget.gadgetbridge.action.SET_DEVICE_SETTING" \
    -e "device" "xx:xx:xx:xx:xx" \
    -e "key" "zepp_os_experimental_features" \
    -e "value" "true"
```

## Development

### Decoding workout summary

The workout summary consists of 2 bytes containing the summary version, followed by a protobuf encoded payload.

You can decode it using the following, for version 0x0080:

```bash
cat null_summary.bin | \
    dd bs=1 skip=2 | \
    protoc --decode WorkoutSummary --proto_path ~/workspace/Gadgetbridge/app/src/main/proto huami.proto
```

### Changing the region

You can use the [[Intent-API#device-settings-intent-api]] to change the device region, by setting the `device_region` string preference (483ef27a4f6c5822bbb7ce415f952457d1248f4). The region should be a 2-letter code (eg. `de` for Germany).

The value defaults to `unknown`. The region affects some available preferences, such as alexa.
