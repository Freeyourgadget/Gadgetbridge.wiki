## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/wearables/sma/

# Current Status

GB support for SMA devices is highly WIP currently.

## Supported devices

### SMA-Q2
This device is supported with the custom firmware [SMA-Q2-OSS](https://github.com/Emeryth/sma-q2-oss) (see also [this project](https://hackaday.io/project/85463-color-open-source-smartwatch)). 

#### Supported Features
* Discovery and pairing
* Notifications

### SMA-Q3
Although there is [work on a custom firmware](https://hackaday.io/project/175577-hackable-nrf52840-smart-watch), there is no direct support for this device yet. Since its also running Espruino, maybe Bangle.js works enough.

