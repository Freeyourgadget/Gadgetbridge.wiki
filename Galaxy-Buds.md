---
gitea: none
include_toc: true
---

## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/headphones/samsung/


## Galaxy Buds

Support for the original Galaxy Buds (2019).

<img src="images/galaxy_buds01.jpg" alt="galaxy buds" width="200">

### Supported features

- Factory reset (in Debug options in Gadgetbridge)
- Battery readout and plotting for each of the buds. This version of earbuds does not report case battery values.
- Find buds function
- Ambient mode + tuning
- Equalizer + tuning
- Touch lock
- Touch events - long press: Voice Assistant, Volume, Quick ambient and Ambient sound.
- Game mode (no idea if this works, it does not seem to do anything)

## Galaxy Buds Live

<img src="images/galaxy_buds_live.jpg" alt="galaxy buds live" width="200">

### Supported features

- Factory reset (in Debug options in Gadgetbridge)
- Battery readout and plotting for each of the buds and for the case.
- Find buds function
- Active Noise Canceling
- Equalizer
- Touch lock
- Touch events - long press: Voice Assistant, Volume, Active Noise Canceling
- Game mode (no idea if this works, it does not seem to do anything)
- Pressure relief with Ambient Sound

## Galaxy Buds Pro

<img src="images/galaxy_buds_pro.png" alt="galaxy buds pro" width="200">

### Supported features

- Factory reset (in Debug options in Gadgetbridge)
- Battery readout and plotting for each of the buds and for the case.
- Find buds function
- Noise control:
    - Active Noise Canceling
        - Level
    - Ambient Sound
        - Volume
- Equalizer
- Touch events
    - Switch Noise/Voice Assistant/Volume/Spotify
        - Switch control
            - Noise canceling/Ambient
            - Noise canceling/Off
            - Ambient/Off
    - Lock
    - Double tap edge
- Settings
    - Ear detection
    - Seamless switch
    - Noise control with one bud
    - Balance
    - Game mode
- Ambient sound
    - Voice detect
    - During call
    - Volume L/R
    - Tone

## Pairing

To add to Gadgetbridge, remove the buds from Android pairing and use Gadgetbridge to scan and then add it in. Allow it to pair. If you have issues pairing, make sure Wear app is either disabled or not installed.

## Protocol

The protocol is partially described in [this document here](https://github.com/ThePBone/GalaxyBudsClient/blob/master/GalaxyBudsRFCommProtocol.md) in the [GalaxyBudsClient](https://github.com/ThePBone/GalaxyBudsClient) project.

The battery values are retreived based on this [python script](https://github.com/ThePBone/GalaxyBuds-BatteryLevel/blob/master/buds_battery.py). There is even a [Gnome Shell extension](https://github.com/sidilabs/galaxybuds-gnome-extension) for the buds and it uses the same python script, adding here just for completness. The most important find is this issue: https://github.com/ThePBone/GalaxyBudsClient/issues/8 , which describes why the methods used to retrieve the battery levels in the python script are different then those used in the protocol description. The [GalaxyBudsClient](https://github.com/ThePBone/GalaxyBudsClient) uses a special debug option to retrieve more data, thus the message_ids are different then the normal way of doing this. This is not important in Gadgetbridge as we only retrieve the battery values.

RFComm message [format](https://github.com/ThePBone/GalaxyBudsClient/blob/master/GalaxyBudsClient/Message/SPPMessage.cs).

[Command IDs](https://github.com/ThePBone/GalaxyBudsClient/blob/master/GalaxyBudsClient/Message/SPPMessagePrivate.cs).

It is especially good to have support for this gadget, as not only the original app is not free but it event prevents you from using it if you free your phone:

<img src="images/wear.png" alt="wear not allowint you to run it due to root or unsupported phone" width="200">
