---
gitea: none
include_toc: true
---

## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/wearables/withings/

# Withings Steel

## Supported features

Note: this list has been created from source code of the implementation.

- Pairing
- Set time
- Watch hands calibration
- Language settings
- Set workout activity types (more then 30)
- Sleep and REM sleep
- Steps tracking
- HR measurements
- Activity tracking (steps, hr, duration, distance)
- Alarms (3) with description and smart wake up


TODO: if you have the watch, fill in more details.
