[WARNING]: <> (Do not rename this page, it is linked from the app!)

## CAUTION

This feature **has the potential to brick your device**! That said, this has never happened to any of the developers through flashing, but remember **you're doing this at your own risk**.

## "Firmware" files

Smart devices can support various different types of files, like firmware updates, watchfaces, fonts, resource files, GPS data, images and so on.

## Getting the "firmware" files

Since we may not distribute the firmware you will have to do a little work to get the files. This means that you will need to search for the files in apk files, online, in forums, on Amazfitwatchfaces (for Miband/Amazfit devices) and so on.
 
## Related pages

- [[Mi Band 2 Firmware Update]]
- [[Mi Band 3 Firmware Update]]
- [[Mi Band 4 Firmware Update]]
- [[Amazfit Bip Firmware Update]]
- [[Mi Band 5 Firmware Update]]
- [[Mi Band 6 Firmware Update]]
- [[Amazfit Band 5 Firmware Update]]
- [[Amazfit Bip S Firmware Update]]
- [[Amazfit Bip U Firmware Update]]
- [[Amazfit Cor Firmware Update]]
- [[Amazfit Cor 2 Firmware Update]]
- [[Mi Band Firmware Update]]
- [[Pebble Firmware updates]]
- [Fossil Hybrid HR](Fossil-Hybrid-HR#user-content-known-firmware-versions)