---
gitea: none
include_toc: true
---

## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/thermometers/femometer/


# Femometer Vinca II

A thermometer to measure body temperature. Support added in #3369

As of right now, the temperature is persisted in the Gadgetbridge database, but there is no user interface to see it.

## Supported features

- Device info
- Battery
- Temperature
- Set time
- Alarm
- Configuration options
  - Measurement mode (quick/normal/precisa)
  - Volume
  - Temperature unit
