## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/wearables/xiaomi/#mi-band-5

Essentially the same as [[Amazfit Band 5]] but without the extra VO2 max sensor.

### Contents
* [[Pairing]]
* [Gadgetbridge Configuration](Configuration#mi-band--amazfit-specific-settings)
* [Weather information](Weather)
* [Alarms](Huami-Alarms)
* [Mi Band 5 Firmware Update](Mi-Band-5-Firmware-Update)
* [Custom watchface](Watchfaces)
* [Starting Activity from the band](Sports-Activities-Workouts#user-content-bands-without-integrated-gps)
* [Heart Rate 3rd party apps data sharing](Heart-Rate-data-sharing)
* [Sleep Detection](Huami-Deep-Sleep-Detection)

