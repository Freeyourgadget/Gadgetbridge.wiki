![Gadgetbridge](https://raw.codeberg.page/Freeyourgadget/Gadgetbridge/@master/app/src/main/res/drawable-mdpi/ic_launcher.png)<br>
----

**NEW WEBSITE**
 - [[New Website]]

**General**
 - [[Home]]
 - [[FAQ]]
 - [ReadMe](https://codeberg.org/Freeyourgadget/Gadgetbridge/src/README.md)
 - [[Configuration]]
 - [[Notifications]]
 - [ChangeLog](https://codeberg.org/Freeyourgadget/Gadgetbridge/src/CHANGELOG.md)
 - [[Widget]]
 - [[Weather]]
 - [Data Backup](Data-Export-Import-Merging-Processing#user-content-backup-your-data)
 - [[Pairing]]
 - [[Find phone]]
 - [[Music info]]
 - [[Permissions Explained]]
 - [[Firmware Update]]
 - [Automation via Intents](Intent-API)


**Sports/Activities**
- [[Sports Activities Workouts]]
- [[Activity Sessions List]]
- [[Activity and Sleep Charts]]
- [Heartrate measurement](Huami-Heartrate-measurement)
- [Integrating Sports Tracking apps with Gadgetbridge Sports Activities/Workouts](Integrating-with-Sports-Tracking-apps)

**Smart Device Related**
 - [[Bangle.js]]
 - [Casio devices](Casio)
     - [Casio GB-5600B](Casio-GB-5600B%2FGB-6900B%2FGB-X6900B%2FSTB-1000)
     - [Casio GB-6900B](Casio-GB-5600B%2FGB-6900B%2FGB-X6900B%2FSTB-1000)
     - [Casio GB-X6900B](Casio-GB-5600B%2FGB-6900B%2FGB-X6900B%2FSTB-1000)
     - [Casio GBD-200](Casio-GBX-100%2FGBD-200)
     - [Casio GBD-H1000](Casio-GBX-100%2FGBD-200)
     - [Casio GBX-100](Casio-GBX-100%2FGBD-200)
     - [Casio STB-1000](Casio-GB-5600B%2FGB-6900B%2FGB-X6900B%2FSTB-1000)
 - [[FitPro]]
 - [[Fossil Hybrid HR]]
 - [[Garmin devices]]
 - [[HPlus]]
 - [Huami devices](Mi-Band)
    - [[Amazfit Active]]
    - [[Amazfit Active Edge]]
    - [[Amazfit Balance]]
    - [[Amazfit Band 5]]
    - [[Amazfit Band 7]]
    - [[Amazfit Bip]]
    - [[Amazfit Bip Lite]]
    - [[Amazfit Bip S]]
    - [[Amazfit Bip U]]
    - [[Amazfit Bip 3 Pro]]
    - [[Amazfit Bip 5]]
    - [[Amazfit Cheetah]]
    - [[Amazfit Cheetah Pro]]
    - [[Amazfit Cor]]
    - [[Amazfit Cor 2]]
    - [[Amazfit Falcon]]
    - [[Amazfit GTR]]
    - [[Amazfit GTR 3]]
    - [[Amazfit GTR 3 Pro]]
    - [[Amazfit GTR 4]]
    - [[Amazfit GTR Mini]]
    - [[Amazfit GTS]]
    - [[Amazfit GTS 3]]
    - [[Amazfit GTS 4]]
    - [[Amazfit GTS 4 Mini]]
    - [[Amazfit Neo]]
    - [[Amazfit T-Rex]]
    - [[Amazfit T-Rex 2]]
    - [[Amazfit T-Rex Ultra]]
    - [[Mi Band 1]]
    - [[Mi Band 2]]
    - [[Mi Band 3]]
    - [[Mi Band 4]]
    - [[Mi Band 5]]
    - [[Mi Band 6]]
    - [[Mi Band 7]]
 - [[MyKronoz ZeTime]]
 - [[Pebble]]
 - [[PineTime]]
     - [InfiniTime](https://codeberg.org/Freeyourgadget/Gadgetbridge/wiki/PineTime#infinitime)
 - [[Sony Wena 3]]
 - [[SMA]]
 - [[WithingsSteel]]  

**Wireless Earbuds**
 - [[Galaxy Buds]]
 - [[Nothing Ear (1)]]
 - [[Sony Headphones]]

**Others**
- [[iTag]] Keyring trackers
- [[Nut]] Keyring trackers
- [[UM25]] USB Voltage meter
- [[VESC]] BLDC controller VESC
- [[Flipper Zero]] Multi-tool Device for Geeks
- [[Roidmi]] Roidmi/Mojietu FM Trans.
- [[Vibratissimo]] Private toy
- [Shell Racing](SuperCars) Toy RC cars
- [[Femometer Vinca II]]

<small>[Full list of supported devices](https://codeberg.org/Freeyourgadget/Gadgetbridge#supported-devices)</small>

**Development**
 - [[How to Release]]
 - [[Developer Documentation]]
 - [[BT Protocol Reverse Engineering]]
 - [[Support for a new Device]]
 - [[New Device Tutorial]]
 - [[Translating Gadgetbridge]]
 - [[OpenTracks-API]]
 - [[Intent-API]]

**Feature Discussion**
 - [[Brainstorming new UI]]
 - [[Activity Analysis]]

**FAQ**
 - [Frequently asked questions](FAQ)
 - [[Log Files]]
 - [Data Export, Import and Processing](Data-Export-Import-Merging-Processing)
 - [[Multiple devices]]
 - [When will a new release appear on f-droid](When-will-a-new-release-appear-on-f-droid)
 - [SMS notifications are lost somewhere](Notifications#sms-notifications-are-lost-somewhere)
 - [Incoming call notification on Lineage OS](Notifications#incoming-call-notification-on-lineage-os)
 - [[Failed to start background service]]
 
