## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/wearables/casio/

The Casio GBX-100, GBD-200 and GBD-H1000 are the second Casio watch generation supported by Gadgetbridge. Please keep in mind that, due to its recent addition, not all features are available.

## Pairing

Make sure the delete the pairing information from the watch and the phone. If you have the Casio app installed, uninstall it before using Gadgetbridge.
During the pairing process, the information stored in Gadgetbridge (your profile) is transferred to the watch and **the information on the watch is overwritten**. Also, the time is synchronized to the watch - this cannot be disabled and is a limitation of Casio's pairing protocol.
Since Gadgetbridge only stored the year of birth, day and month are set to January 1. This is the only information that you can safely change on the watch without being overwritten by Gadgetbridge.

## Supported Features

So far, Gadgetbridge supports the following subset of the watch's features:

  * Pairing
  * Automatic Reconnect
  * Profile Synchronization
  * Setting step count and energy target (calculated based on the profile information)
  * Notifications
  * Phone finder

#### Currently WiP:

  * Step Counter

#### Low Priority:

  * Exercise Data
  * Heartrate Data (GBD-H1000 only)
  * GPS Data (GBD-H1000 only)

#### No Priority:

  * Tide Data (GBX-100 only)
  * Sunset/Sunrise Data
  * World Time

## Profile Synchronization

The synchronization is set up in the "Gadgetbridge-always-wins"-style. This means that all information changed on the watch will be overwritten by Gadgetbridge upon the next reconnect.

The units are currently not synchronized. Other units than metric units have not been tried, although this part of the protocol is fully understood.

## Supporting more features

Help is wanted, especially regarding Exercise/Heartrate/GPS data. Bluetooth HCI Logs from the Casio app including the representation in the Casio UI would be very much appreciated!