## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/wearables/amazfit/#balance

See the [Zepp OS](Zepp-OS) page for a list of features and issues common to Zepp OS devices.

**Warning**:  this is a Zepp OS 3.0 device, which is still in the early stages of development. See the page above for known issues specific to Zepp OS 3.

### Tested Firmwares

- Unknown

### Tested Hardware

- Unknown
