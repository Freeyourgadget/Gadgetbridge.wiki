---
gitea: none
include_toc: true
---

## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/basics/features/activities/

In order to receive data, make sure that no other app is syncing and removing
the data from your device, so make sure that the official app is not running.
Kill the app (this might be not possible as every notification might start it
up again) or uninstall it.

### Synchronisation of steps and sleep

To synchronize/fetch steps and sleep data from the watch/band to Gadgetbridge,
you can press the Sync icon on the Device card in the Control panel. (Some
devices push this data to Gadgetbridge automatically (like Pebble) so the Sync
icon is not present).

<img src="images/sync_activity.png" alt="sync activity" width="200">
<br/>

### Charts access

To access the Activity charts, use the Charts icon:

<img src="images/activity_charts.png" alt="Activity charts" width="200">
<br/>

### Charts settings

The Charts section in Gadgetbridge provides comprehensive overview of sleep time, activity time, number of steps and so on. Visibility and position of each tab can be adjusted on per device basis in <img src="images/ic_device_preferences.jpg" alt="device preferences icon" width="20"> Device specific preference settings:

<img src="images/device.jpg" alt="device" width="200">
<br/>

<img src="images/charts/ChartsTabsSettings.jpg" alt="charts tabs settings" width="200">

## Charts

### Activity and sleep

This displays activity intensity (movement) on Y axis (green during the dy, blue during night), heart rate on Y axis (orange), time on X axis:

<img src="images/charts/ActivityAndSleep.png" alt="charts image sleep" width="200">

### Sleep

Detailed view just for the past night. You can adjust the view interval as  `Noon to noon` or `Past 24 hours`. The circular chart shows light/deep sleep break down. Do note, that for most watches, the deep sleep detection is not reliable. But, what you mostly care about is heart rate, often this can be enabled as `Continuous HR measurement` or `Improve sleep detection by using HR` in settings. Minimum/maximum and average heart rate is displayed in the heart icon. Also, movement intensity (the blue lines on Y axis) is displayed as total for the night in the intensity icon.

<img src="images/charts/Sleep.png" alt="charts image sleep" width="200">

### Activity Sessions

See more details in [[Activity Sessions List]]

<img src="images/charts/ActivityList.jpg" alt="charts image list" width="200">

<img src="images/charts/Activity_list_dashboard.jpg" alt="charts image list dashboard" width="200">

### Sleep per week/month

The Y axis shows light/deep sleep, make sure to add them up for total amount of sleep. Average and over/lack of sleep is displayed.

<img src="images/charts/SleepPerWeek.png" alt="charts image sleep" width="200">

### Steps per week/month

<img src="images/charts/StepsPerWeek.png" alt="charts image steps" width="200">

<img src="images/charts/StepsPerMonth.png" alt="charts image steps" width="200">

#### Steps Streaks

The Steps per month now provide new icon to access Steps Streaks screen:

<img src="images/charts/StreaksIcon.jpg" alt="steps streaks icon" width="200">

Steps streaks screen shows ongoing (if present), longest steps streak (series
of days without interruption with the daily step goal reached) and also total
statistics of daily goals success rate.  You can use long press on some text
elements to read more details, for example the total steps per day average.

<img src="images/charts/StepsStreaks.jpeg" alt="steps streaks" width="200">

### Speed zones

See breakdown by steps per minute.

<img src="images/charts/SpeedZones.png" alt="charts image live activity" width="200">

#### Live activity

You can get a chart of life values (if your device supports that).

<img src="images/charts/LiveActivity.png" alt="charts image live activity" width="200">

### Stress

The stress screen provides with an overview of the stress levels during the day, as well as the average stress and breakdown of time for each stress level.

<img src="images/charts/Stress.jpg" alt="charts image stress" width="200">

### PAI

PAI (Personal Activity Intelligence) consists of a score computed by some sports watches and bands, based on the last 7 days of activity. The expected goal is to maintain a score of 100 PAI or higher. PAI points are earned by elevating the heart rate. The PAI screen provides with an overview of the daily PAI score, as computed by the watch.

<img src="images/charts/PAI.jpg" alt="charts image pai" width="200">
