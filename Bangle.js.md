## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/wearables/banglejs/

"Bangle.js support came directly from Gordon Williams of Espruino! And marks a special milestone for Gadgebridge. It is the first time that Gadetbridge support was contributed directly from the creators of a device! The Bangle.js is a fully hackable smartwatch with an open source firmware and apps written in JavaScript. For more information visit banglejs.com."
Source: https://blog.freeyourgadget.org/release-0_40_0.html

For more info on setting up, see http://www.espruino.com/Gadgetbridge

## Supported devices

* [Bangle.js 1](https://www.espruino.com/Bangle.js)
* [Bangle.js 2](https://www.espruino.com/Bangle.js2)
* Any device with a 'Nordic UART' service (any Bluetooth LE Espruino device, or Adafruit Bluefruit, etc) that is able to decode the JSON packets documented at http://www.espruino.com/Gadgetbridge

## Protocol

The Bangle.js integration for Gadgetbridge sends JSON-formatted packets over a 'Nordic UART' serial service on Bluetooth LE.

Sent JSON is wrapped in `GB({ ... })\n` so that a function called `GB` on Bangle.js can be called via the REPL, and received data is sent as raw newline-separated JSON.

More info on the packet formats (and usage) is at: http://www.espruino.com/Gadgetbridge

## Build Flavor

There is a `banglejs` build flavor for Gadgetbridge that tweaks the build slightly - allowing internet access and renaming the app to `Bangle.js Gadgetbridge` to avoid confusion with the default app.