## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/wearables/amazfit/#bip

Contents
* [Amazfit Bip status](#amazfit-bip-status)
* [Getting started](#getting-started)
* [[Notifications]]
* [[Pairing]]
* [aGPS data for GPS chip](HUAMI-GPS)
* [Alarms](Huami-Alarms)
* [Gadgetbridge Configuration](Configuration#mi-band--amazfit-specific-settings)
* [Firmware Update](Amazfit-Bip-Firmware-Update)
* [BipOS](Amazfit-Bip-OS)
* [Weather information](Weather)
* [Activity List](Activity-List)
* [Sports Activities - Workouts](Sports-Activities-Workouts)
* [Custom watchface](Watchfaces)
* [Heartrate measurement](Huami-Heartrate-measurement)
* [Heart Rate 3rd party apps data sharing](Heart-Rate-data-sharing)
* [Button Actions](Huami-Button-Actions)
* [Device Actions](Huami-Device-Actions)
* [Sleep Detection](Huami-Deep-Sleep-Detection)

## Amazfit Bip status

This device acts almost like the Mi Band 2, and almost all features that work on the Mi Band 2 already work for Amazfit Bip. 

### Additional features

* Rejecting phone calls
* Ignoring calls
* Weather forecast
* Full text notifications with icons
* Export of activities with GPS and HR data
* Flash new watchfaces (with the firmware update tool)
* Use button for actions

### Features that work on Mi Band 2 but not on the Bip:

* Reboot (debug menu)

## references to other resources
https://github.com/amazfitbip/

## Getting started

When powered up for the first time, the device demands that you connect in via the official App. This is not necessary, you can get started with Gadgetbridge only.

Steps to follow:

* Install Gadgetbridge
* If you had your watch previously paired to your phone's bluetooth, make sure to unpair it first.
* When starting Gadgetbridge the first time, it will automatically attempt to discover and pair your Amazfit Bip. When your Bip notifies the pairing process, tap it quickly a few times in a row to confirm the pairing.


