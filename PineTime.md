## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/wearables/pine64/

## PineTime

At this point, [PineTime](https://www.pine64.org/pinetime/) by [Pine64](https://www.pine64.org/) seems to have many developmental versions of firmware and the communication between watch/mobile device is not streamlined. Adding support for many versions of PineTime-foo and PineTime-bar are possible, but long term maintenance will be an issue. Merging these efforts would be best.

Initial PineTime support related conversation is in the [Issue tracker](https://codeberg.org/Freeyourgadget/Gadgetbridge/issues/1824)


### WASP

Wasp-os is a firmware for smart watches that are based on the nRF52 family of microcontrollers, and especially for hacker friendly watches such as the Pine64 PineTime. See more details [here](https://wasp-os.readthedocs.io/en/latest/README.html).

### InfiniTime

We have added initial support for [InfiniTime](https://github.com/JF002/Pinetime). It supports time setting and observing hardware and software version.

#### Pairing
- Secure pairing
    -  Only on InfiniTime 1.8.0 and above
    -  When pairing, a prompt will pop up asking you if you want to pair the device. If you select Don't pair the device will connect without any special procedure. This option will be removed in the future. If you select pair your phone will ask you for a special code displayed on the watch itself. After enetering the phone will make a bond with the watch.
- Unsecure pairing 
    - This allows you to connect your watch to Gadgetbridge without any special procedure. It also allows anyone else to connect to it as well, so this option will be removed in the future.


#### Steps sychronisation

- Steps sync works only since InfiniTime 1.7
- InfiniTime advertise "steps" info when the PineTime screen is ON (and a bit after that). Hence:
    - to not loose your latest steps you should unlock the PineTime screen before the end of the day and also in the early morning
    - when the PineTime screen is ON and you are moving, PineTime will send "steps" count every about 2-10 seconds, and Gadgetbridge may start to treat this data as an Activity (and also displaying it in Activity charts). that data and charts will not be accurate: you should wait for ["Health/Fitness data storage and expose to companion app](https://github.com/InfiniTimeOrg/InfiniTime/projects/4)" project to be implemented on the PineTime side. and meanwhile, in Gadgetbridge open "Device specific settings" and change/uncheck option in "Charts tabs" and "Activity info on device card" to leave only Steps data.

    <img src="https://codeberg.org/attachments/2bebe8ba-7dbd-4f71-92d3-bc7d6bf50c10" width=200 /> <img src="https://codeberg.org/attachments/0df9e607-2eda-449a-b1ab-ce1b63336487" width=200 />
    
#### Battery level
- Gadgetbidge records and saves the data about your battery precentage estimated on the battery voltage. This data can later be shown in a graph.

#### Find lost device
- Same as many other devices, Pinetime can be found with the Find lost device button. This will simulate a phone call made by Gadgetbridge and ring the watch.

#### Special characters in notifications
- Gadgetbride offers transliteration to correctly display text in notifications. This option can be enabled in the device settings

#### Calls
- Gadgetbridge with Infinitime support answering and declining a phone call right from your wrist

#### Music control
- Music or other media can be controlled from the music app in Infinitime. It allows you to skip, play/pause and play previous song

#### Heartrate monitor
- As of 1.8.0, Infinitime only supports manual hr measurments. This data is not send to Gadgetbridge and is not displayed in the graph


#### Sleep tracking
- Infinitime currently does not support sleep tracking.
 

