## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/wearables/xiaomi/#mi-band-6

[WARNING]: <> (Do not rename this page, it is linked from the app!)

### Contents
* [[Pairing]]
* [Gadgetbridge Configuration](Configuration#mi-band--amazfit-specific-settings)
* [Weather information](Weather)
* [Alarms](Huami-Alarms)
* [Mi Band 6 Firmware Update](Mi-Band-6-Firmware-Update)
* [Custom watchface](Watchfaces)
* [Starting Activity from the band](Sports-Activities-Workouts#user-content-bands-without-integrated-gps)
* [Heart Rate 3rd party apps data sharing](Heart-Rate-data-sharing)
* [Sleep Detection](Huami-Deep-Sleep-Detection)

#### New auth protocol
[WARNING]: <> (Do not rename this section, it is linked from the app!)

If you get a "Update the app to latest version" message on the band, make sure to check the "New Auth Protocol" in the device settings in Gadgetbridge:

<img src=images/mb6_device_settings.jpg width=200>

<img src=images/mb6_new_auth_protocol.jpg width=200>

If you are pairing the device for the first time, you can get to the Setting screen by long press on the device in the Discovery screen:

<img src="images/pairing/pairing-huami.jpg" alt="pairing huami" width="350"/>

