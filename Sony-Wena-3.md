---
gitea: none
include_toc: true
---

## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/wearables/sony#wena-3


Support for the Sony Wena 3 was added in #3311. Manufacturing has stopped and official support will be phased out over the coming years.

## Current status
* Bonding/pairing works
* Serial/firmware version
* Music Player screen
* Weather
* Notification forwarding with app-specific pattern settings
* Detect "Photography Pro" to enable the Xperia-specific camera control mode
* Display settings (language, power on on wrist flick, font size, etc)
* Menu and home screen customization
* Alarms
* Calendar sync
* Do Not Disturb
* Auto Power Off
* Button binding (long press / double click)
* LED color and vibration setting (for all apps at once)
* Health data sync seemingly works?
* Volume buttons, camera buttons, media buttons — they seem to Just Work™ through the HID profile of the device.

## Not implemented
* IC/iD/QuikPay support
* Alexa support
* Qrio support
* Mamorio support
* Riiiver support
* Seiko head linkage function
* Firmware update

## Known issues
* Sometimes when going out of range for a long time, need to connect the device from Android settings, otherwise Camera and Music controls don't work
* Sleep data is flaky - #3338
* Non-ASCII notifications sometimes don't work well - #3359
* Battery drain seems to be much higher than with the official app