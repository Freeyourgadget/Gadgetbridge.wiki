## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/basics/pairing/

[WARNING]: <> (Do not rename this page, it is linked from the app!)
## Pairing

Read this whole info to the end. These instructions are known to work but not following the steps exactly will cause issues.

Pairing establishing a connection between two Bluetooth devices. It can be a complex process and the type of pairing can determine what possibilities are given to the connected device during day-to-day operation.


### Preparations and simple steps

- unpair the band/watch from your phone's bluetooth
- if you are pairing MiBand/Amazfit device, read about [Huami Server Pairing](#huami-server-pairing)
- add the device to Gadgetbridge by using the `Add new device` Plus button.

### Pairing in Android bluetooth settings

Before you add the device to Gadgetbridge, it is recommended to unpair the band/watch from your phone's bluetooth. If your device settings then give you a chance to Enable Bluetooth pairing (Mifit/Amazfit devices do offer this, make sure that "Gadgetbridge → Settings → MiBand/Amazfit settings → Enable Bluetooth pairing" is checked) then do enable it, so your device is added to Gadgetbridge AND also paired in the Android pairing of your phone). The Android bluetooth pairing is recommended because it allows Gadgetbridge to reconnect a disconnected device in a much better way, without having to scan for the device and it ensures that for example Miband/Amazfit devices can reconnect after phone reboot. The [Companion Device pairing](#companion-device-pairing) extends trust between the pairded device and it is recommended to have it enabled as well. If you experience issues when adding/connecting to the device, try disabling this option.


### Extra pairing settings/known issues

In the Discovery and Paring options (available from Device discovery as well as from Settings) the  there are few options that are related to pairing. **If your scan never finishes and seems to cause Gadgetbridge to freeze and become non responsive**, check the `Disable new BLE scanning`. You can also re-try the pairing in a place with less Bluettoth traffic.

On Android 8.0 (API level 26) and higher, to make sure that Gadgetbridge has the possibility to use "Find phone" and so on you should check the `Companion Device Pairing`, see more details about [Companion Device pairing](#companion-device-pairing).


## [[Pairing types and Auth Key]]

This describes types of pairing and how Gadgetbridge uses an auth key as a secret. Read more [here](Pairing-types-and-Auth-Key).

## [[Huami Server Pairing]]

Some device (Mi Bands, Amazfit watches and perhaps other brands too) from certain version of their firmware **require** server based pairing for the very first initialization of the device. For example Mi Band 3 does not require this. Mi Band 4, Mi Band 5 and devices released after them do require this process of pairing. This means that for devices that require server base pairing you absolutely must at least once use the original MiFit, Amazfit, Zepp or other official app to make the very first, initial pairing. After you retrieve the pairing key and then use this key to pair with Gadgetbridge you can (and you should) remove the original app. Read more [here](Huami-Server-Pairing).

## [[Companion Device pairing]]

Companion Device Management is a new Android way to establish further entrusted connection between devices. It is eseential on Android 10 and up as it provided better environment for Gadgetbridge to run in the background, allow it to launch activities from background - for example during the [[Find phone]] command, which does not work if Companion Device pairing is not used. Read more [here](Companion-Device-pairing).

## [[Pebble Pairing]]

Pebble and Pebble2 related tips for the pairing process.