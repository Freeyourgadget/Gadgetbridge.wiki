## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/basics/topics/background-service/

You're on this page probably because you saw a notification with the content being something like:

> Failed to start background service  
> Starting the background service failed because…

This notification means that Gadgetbridge was prevented from starting the background service it needs in order to function properly.

Possible fixes:

* Enable "Autostart" under the application's settings available in the Settings **of your phone**
* Enable [CompanionDeviceManager Pairing](https://codeberg.org/Freeyourgadget/Gadgetbridge/wiki/Companion-Device-pairing) in Gadgetbridge's settings (available if you have Android 8 or up)
* Disable battery optimizations on Gadgetbridge
* Allow "Start on boot" under "Other permissions" if you have a Xiaomi phone

If none of those work and you still see this notification, please open an issue.