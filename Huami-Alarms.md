## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/basics/features/alarms/

## Alarms

### Alarms database syncing

The Alarm database in Gadgetbridge is updated during Bluetooth connect - information about alarms is pulled from the band/watch. Only alarms' status can be retrieved, but not their times. Database of alarms is sent to the device as you exit the Edit alarms activity in Gadgetbridge.

### Enabling/disabling alarms in the gadget

When alarms are enabled/disabled on the device, info is pushed to Gadgetbridge either right away (while connected) or on next connect (as per above).


### Marking alarms as unused in Gadgetbridge

Alarms disabled via Gadgetbride are also sent to the device, which can make it a bit confussing and cluttered especially on smaller screen bands (MB3, MB4...), but it is beneficial on devices, where alarms can be enabled/disabled on the device itself (Bip, MB4). To hide unused alarms, alarms can be set as *unused*, via **long tap** on the alarm in the alarm list. These alarms are then not shown in the device.