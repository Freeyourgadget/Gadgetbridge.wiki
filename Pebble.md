## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/wearables/pebble/

### UserLand
In this section you find pages relevant to end-users:

- [[Pebble Pairing]]: Pebble and Pebble2 pairing tips
- [[Pebble Getting Started]]: first steps with your Pebble and Gadgetbridge
- [[Pebble Firmware updates]]: firmware downloads for Pebble devices
- [[Pebble Language Packs]]: language pack download and installation
- [[Pebble Watchfaces]]: apps on your Pebble (aka Watchfaces), where to find, how to use/manage, known issues/limitations
- [Weather information](Weather)
- [[Pebble System Apps]]: pre-installed system apps supported via Gadgetbridge
- interacting with [[Calls and SMS]]
- [[PebbleKit Android App Compatibility]]: other Pebble companion apps with the known state of their compatibility with Gadgetbridge
- [[Pebble-2-BTLE-specifics]]
- [[Miscellaneous Pebble Tips]]
- [[Pebble Links]]: useful links related to your Pebble

### Technical docs
The following information is more technical (API/protocol descriptions etc.) and targeted at developers. If you just want to use Gadgetbridge, you won't need to follow below links – but of course you're welcome to do so if you want to take a look "behind the scenes".

- [[Pebble Datalog]]: Background on the Pebble Datalog (format, how it works, etc.)
