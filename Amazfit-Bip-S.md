## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/wearables/amazfit/#bip-s

## Amazfit Bip S

Contents
* [[Notifications]]
* [[Pairing]]
* [aGPS data for GPS chip](HUAMI-GPS)
* [Alarms](Huami-Alarms)
* [Gadgetbridge Configuration](Configuration#mi-band--amazfit-specific-settings)
* [Firmware Update](Amazfit-Bip-S-Firmware-Update)
* [Weather information](Weather)
* [Activity List](Activity-List)
* [Sports Activities - Workouts](Sports-Activities-Workouts)
* [Custom watchface](Watchfaces)
* [Heartrate measurement](Huami-Heartrate-measurement)
* [Heart Rate 3rd party apps data sharing](Heart-Rate-data-sharing)
* [Device Actions](Huami-Device-Actions)
* [Sleep Detection](Huami-Deep-Sleep-Detection)

The Amazfit Bip S is an improved Version of the original Amazfit Bip. It has a few hardware changes and software improvements:
- 64 (RGB222) insted of 8 colors (RGB111), however the new screen is not as readable in low-light environments as the old one, but looks much nices when the background light is enabled than the original Bip screen.
- Music controls officially supported
- Better HR Sensor
- World clock (not yet supported in Gadgetbridge)
- Many more activities supported
    - including swimming
- 5ATM water resistance
- Bluetooth 5.0
- (probably more)

There are a few protocol changes in comparison to the original Bip, and support in Gadgetbridge is not yet on par with the original Amazfit Bip. Make sure to always use the latest Gadgetbridge release. Bip S is working mostly in the same way regular [Amazfit Bip](Amazfit-Bip) does, so please also refer to it's wiki page.



