## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/basics/pairing/companion-device/

On phones running Android 8.0 (API level 26) and higher, it's possible to use companion device pairing. It is recommended to use this setting, especially on Android 10 and higher as without it for example the Find phone feature cannot work correctly.

After the device is paired using this method, the device can leverage the `REQUEST_COMPANION_RUN_IN_BACKGROUND` permission to start the app from the background. This provides a better way for Gadgetbridge to keep its service running, read more about background service stability [here](https://codeberg.org/Freeyourgadget/Gadgetbridge/wiki/Failed-to-start-background-service).

In the future there might be a possibility to use companion device pairing to perform a Bluetooth (or Wi-Fi) scan of nearby devices, without requiring the `ACCESS_FINE_LOCATION` or `BLUETOOTH_ADMIN` permission. This would minimize the number of permissions needed. Currently, these permissions are still required. See more details here: https://developer.android.com/guide/topics/connectivity/companion-device-pairing

To utilize this in Gadgetbridge, enable "CompanionDevice Pairing" in the settings:
```
Settings > Developer Options > Discovery and Pairing options > CompanionDevice Pairing
```
and add your new device via the <img src="images/pairing/plus.jpg" alt="plus icon" width="20"> icon.

## Pairing existing devices as companion

For devices already in Gadgetbridge, you must re-pair (re-bond) them:

* Connect to the device:

<img src="images/pairing/companion-pairing-01.jpg" alt="connected" width="200">

* Go to the Debug activity:

<img src="images/pairing/companion-pairing-02.jpg" alt="connected" width="200">

* Click the "Pair current device as companion" button:

<img src="images/pairing/companion-pairing-03.jpg" alt="pair-current-as-companion" width="200">

* Select OK on the Link question:

<img src="images/pairing/companion-pairing-04.jpg" alt="linking" width="200">
