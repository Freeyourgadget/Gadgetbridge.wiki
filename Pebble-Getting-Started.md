## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/basics/pairing/pebble/

All Pebbles when powered up for the first time demand that you connect in via the official App. **This is not necessary, you can get started with Gadgetbridge only**. While on original Pebbles this screen could be bypassed by a long press on the right button, newer pebbles seem to be stuck in recovery until a firmware gets flashed.

We recommend to flash the latest available firmware. On the Pebble and Pebble Steel (black and white models), this includes flashing a migration firmware first.

Steps to follow:
* Install Gadgetbridge
* Pair your Pebble through Gadgetbridge's Control Center (Press the + button on the main Gadgetbridge screen. **Do NOT Select THE Pebble LE**)
* Start Gadgetbridge and tap on your Pebble until it says initialized
* Flash a firmware as noted in [[this wiki article|Pebble Firmware updates]]
* Install a language pack as noted in [[this wiki article|Pebble-Language-Packs]]
* Optionally enable Health if device supports it, see [[Pebble System Apps#health-not-for-pebble-classic-and-steel]]
