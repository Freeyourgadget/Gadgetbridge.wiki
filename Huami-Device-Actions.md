## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/internals/automations/events/

## Configurable device events

Some devices provide information when a particular event has been detected (for example Wake up, Falling asleep). In Gadgetbridge, you can assign actions to these events.

Currently, this is available in MiBand and Amazfit devices (know about more? Please help with edits).

You can configure Gadgetbridge to either trigger a pre-built action or send a broadcast message.

<img src="images/device_actions.jpg" alt="device actions" width="200">

### Gadgetbridge configuration:

* In `Device specific settings` go to `Device actions`.
* Choose an event (for example **On Fall Asleep**) and choose `Run action`. Options include *Media Play*, *Media Pause*, *Toggle media playback* and also *Send Broadcast*.
* If you choose Send Broadcast, you can also customize the broadcast message in the `Broadcast message` menu.
 
### Using broadcast messages

You can use apps like Easer or Tasker to run additional appliacations, command or actions when the Broadcast messages are sent. See [example configuration of Easer for button actions](Huami-Button-Actions#user-content-easer) in a similar topic.


#### Tasker example configuration

This shows how intent broadcasts can be used with Tasker. Please feel free to expand this.

<img src="images/device_actions/tasker01.png" alt="Tasker 1" width="200"/>

<img src="images/device_actions/tasker02.png" alt="Tasker 2" width="200"/>


