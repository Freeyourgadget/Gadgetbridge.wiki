## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/internals/automations/intents/

# Intent API

This page documents some of the Intent APIs available in Gadgetbridge.

## Bluetooth Intent API

This API allows controlling Bluetooth connection via Intents. It must be enabled explicitely in the Settings, by checking the "Bluetooth Intent API" setting.

### Device Connection

When a device is connected, the following Intent is broadcasted:
- **Action:** `nodomain.freeyourgadget.gadgetbridge.BLUETOOTH_CONNECTED`
- **Extra:** `EXTRA_DEVICE_ADDRESS`, containing the device address

In order to trigger a device connection from other app:

```bash
adb shell am broadcast \
    -a "nodomain.freeyourgadget.gadgetbridge.BLUETOOTH_CONNECT" \
    -e "EXTRA_DEVICE_ADDRESS" "xx:xx:xx:xx:xx"
```

## Device Settings Intent API

This API allows for device-specific settings to be set through Intents. It must be enabled explicitely per-device, in the device preferences secreen.

The actual preferences supported by each device may vary. Right now, the only way to know the correct / supported preference keys is by checking the code.

**Warning:** Given the low-level access of this feature, it has the potential for crashing Gadgetbridge, or even the device.

### Examples

Toggling AOD:

```bash
adb shell am broadcast \
    -a "nodomain.freeyourgadget.gadgetbridge.action.SET_DEVICE_SETTING" \
    -e "device" "xx:xx:xx:xx:xx" \
    -e "key" "always_on_display_mode" \
    -e "value" "always"
```

Setting an integer preference (eg. brightness):

```bash
adb shell am broadcast \
    -a "nodomain.freeyourgadget.gadgetbridge.action.SET_DEVICE_SETTING" \
    -e "device" "xx:xx:xx:xx:xx" \
    -e "key" "screen_brightness" \
    --ei "value" "100"
```

## Activity Sync and DB export

It's possible to trigger activity sync and database exports using Intents. All the intents below can be enabled selectively in the **Intent API** section of the Settings screen.

### Trigger activity sync

```shell
adb shell am broadcast \
    -a "nodomain.freeyourgadget.gadgetbridge.command.ACTIVITY_SYNC" \
    nodomain.freeyourgadget.gadgetbridge
```

You can also optionally specify the data types to be synced, as hex values, as defined in [RecordedDataTypes](https://codeberg.org/Freeyourgadget/Gadgetbridge/src/branch/master/app/src/main/java/nodomain/freeyourgadget/gadgetbridge/model/RecordedDataTypes.java):

```shell
adb shell am broadcast \
    -a "nodomain.freeyourgadget.gadgetbridge.command.ACTIVITY_SYNC" \
    -e "dataTypesHex" "0x00000002" \
    nodomain.freeyourgadget.gadgetbridge
```


### Trigger DB export

```shell
adb shell am broadcast \
    -a "nodomain.freeyourgadget.gadgetbridge.command.TRIGGER_EXPORT" \
    nodomain.freeyourgadget.gadgetbridge
```

### Broadcast on database exports

When the database exported, two Intents are broadcast:

* On success: `nodomain.freeyourgadget.gadgetbridge.action.DATABASE_EXPORT_SUCCESS`
* On failure: `nodomain.freeyourgadget.gadgetbridge.action.DATABASE_EXPORT_FAIL`


## Debug Actions Intents

This API allows usage of some custom actions that are also manually accessible from the app's Debug menu. It must be explicitely enabled in Settings, by checking the "Allow Debug Commands" option.

### Send a custom notification

Send a standard notification to your gadget devices, with all fields available for customization.  
All Extra flags are optional; placeholder values will be used when a flag isn't specified.

```shell
adb shell am broadcast \
    -a "nodomain.freeyourgadget.gadgetbridge.command.DEBUG_SEND_NOTIFICATION" \
    -e "type" "GENERIC_SMS" \
    -e "phoneNumber" "0123456789" \
    -e "sender" "Wall Entity" \
    -e "subject" "I live in your walls" \
    -e "body" "I am living in your walls. You may be concerned about this. ..." \
    nodomain.freeyourgadget.gadgetbridge
```

### Trigger a fake incoming call

Make your gadget devices act like a phone call is incoming.  
All Extra flags are optional; placeholder values will be used when a flag isn't specified.

```shell
adb shell am broadcast \
    -a "nodomain.freeyourgadget.gadgetbridge.command.DEBUG_INCOMING_CALL" \
    -e "caller" "Your Nightmare" \
    nodomain.freeyourgadget.gadgetbridge
```

### Change device MAC address

This intent changes a device mac address. It is useful when replacing a device with one of the same model/brand, or when the mac address changes because of a factory reset.

Pre-requisites:
- The old device is still shown in Gadgetbridge
- The new device does **NOT** exist in Gadgetbridge.
- The devices are of same brand and model

This intent will:
- Migrate all preferences from the old to the new mac address, since preferences are saved by mac address
- Update the mac address of the device in the Gadgetbridge database
- Quit gadgetbridge to force a reload


```
adb shell am broadcast \
    -a "nodomain.freeyourgadget.gadgetbridge.command.DEBUG_SET_DEVICE_ADDRESS" \
    -e "oldAddress" "00:00:00:00:00:00" \
    -e "newAddress" "FF:FF:FF:FF:FF:FF" \
    nodomain.freeyourgadget.gadgetbridge
```

Remember to update the auth key for the new device in preferences, if necessary.

## Other Intent based access in Gadgetbridge

See pages below that are describing Intent/Broadcast way of how Gadgetbridge can control other apps or how it can be controlled from other apps.


- [Send notifications to device from other apps](Notifications#send-notifications-to-device-from-other-apps)
- [Bangle.js 2 Intent support](http://www.espruino.com/Gadgetbridge)
- [Fossil Hybrid HR Intent support](Fossil-Hybrid-HR#user-content-using-tasker-easer-or-another-automation-app)
- [Miband/Amazfit send Intent on wake up/sleep detection](Huami-Device-Actions#using-broadcast-messages)
- [Miband/Amazfit send Intent on button press](Huami-Button-Actions#broadcast-message)
- [Flipper Zero Intent examples](Flipper-Zero#using-intent-based-api-to-play-a-file)
