## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/basics/topics/permissions/

Gadgetbridge requires a ton of permissions, because it forwards most of what is going on on the phone to the wearable and therefore it needs access to the data: call logs, phone state, sms... Wearable devices ever since allowed for example calendar syncing, therefore access to calendar must exist and so on. One notable permission is access to location, which is caused by Android permission system: *Because discoverable devices might reveal information about the user's location, the device discovery process [requires location access](https://developer.android.com/guide/topics/connectivity/bluetooth-le.html#permissions)*. Since recent versions of Gadgetbridge, the location permission has been used to provide GPS data to the watch for sports activities, see [below](#used-for-workout-gps-track-recording) and also [here](Sports-Activities-Workouts#gps-data).

To ensure that none of the accessed data is ever forwarded anywhere, Gadgetbridge does not have (and will not have) Android network permission, which means that Gadgetbridge has no means of sending any data anywhere. This is one of the most critical safeguards in the Gadgetbridge  permission system.

## Used for Bluetooth access
These permissions are needed to discover and connect to Bluetooth devices. Because discoverable devices might reveal information about the user's location, the device discovery process [requires location access](https://developer.android.com/guide/topics/connectivity/bluetooth-le.html#permissions). 


- android.permission.BLUETOOTH
    - Allows applications to connect to paired bluetooth devices. https://developer.android.com/reference/android/Manifest.permission#BLUETOOTH
- android.permission.BLUETOOTH_ADMIN
    - Allows applications to discover and pair bluetooth devices. https://developer.android.com/reference/android/Manifest.permission#BLUETOOTH_ADMIN
- android.permission.ACCESS_COARSE_LOCATION
    - Because discoverable devices might reveal information about the user's location, the device discovery process requires location access. https://developer.android.com/guide/topics/connectivity/bluetooth-le.html#permissions

## Since Android 10, more permissions are required

This is to indicate access to user data, such a call log, calls, contacts and more. 

- android.permission.ACCESS_FINE_LOCATION
    - Because discoverable devices might reveal information about the user's location, the device discovery process requires location access. https://developer.android.com/guide/topics/connectivity/bluetooth-le.html#permissions
- android.permission.ACCESS_BACKGROUND_LOCATION
    - "Only when the app is open" is unsuitable because GB needs to sometimes scan in a background worker.
    - Because discoverable devices might reveal information about the user's location, the device discovery process requires location access. https://developer.android.com/guide/topics/connectivity/bluetooth-le.html#permissions
- android.permission.READ_CALL_LOG
- android.permission.READ_PHONE_STATE
- android.permission.PROCESS_OUTGOING_CALLS
- android.permission.ACCESS_NOTIFICATION_POLICY
- android.permission.CALL_PHONE
- android.permission.ANSWER_PHONE_CALLS
    - Call control, such as hanging up
- android.permission.RECEIVE_SMS
- android.permission.SEND_SMS
- android.permission.READ_CONTACTS
- android.permission.READ_EXTERNAL_STORAGE
- android.permission.READ_CALENDAR
    - Calendar event sync
- android.permission.MEDIA_CONTENT_CONTROL

## Used for background service
Top keep the Bluetooth connection running even is the app is not in direct usage by the user at the moment, it needs services to keep it from being killed by the system.

- android.permission.RECEIVE_BOOT_COMPLETED
- android.permission.FOREGROUND_SERVICE
- android.permission.REQUEST_COMPANION_RUN_IN_BACKGROUND
    - This is used to (re)start the background service while also being in the background, read more here [Companion Device Pairing](https://codeberg.org/Freeyourgadget/Gadgetbridge/wiki/Companion-Device-pairing)

## Automatic starting/running

Gadgetbridge services are started automatically on boot, notifications and bluetooth, based on:

- android.permission.RECEIVE_BOOT_COMPLETED
- android.permission.BIND_NOTIFICATION_LISTENER_SERVICE
- android.permission.BLUETOOTH
- android.permission.BLUETOOTH_ADMIN


## Used for reverse find device
- android.permission.VIBRATE

## Used for weather provider access
Weather information is forwarded to Gadgetbridge from 

- cyanogenmod.permission.ACCESS_WEATHER_MANAGER
- cyanogenmod.permission.READ_WEATHER
- lineageos.permission.ACCESS_WEATHER_MANAGER
- lineageos.permission.READ_WEATHER
- org.omnirom.omnijaws.READ_WEATHER

## Used for workout GPS track recording

For workouts started from the band, if supportd by the band and by Gadgetbridge

- android.permission.ACCESS_COARSE_LOCATION
- android.permission.ACCESS_FINE_LOCATION


### Note

- page started based on https://codeberg.org/Freeyourgadget/Gadgetbridge/issues/2062
- permission list and the initial explanation taken from current manifest file
- open for interested users to explore the source code and provide more details