## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/wearables/amazfit/#bip-3-pro

## Amazfit Bip 3 Pro

Contents
* [[Notifications]]
* [[Huami Server Pairing]]
* [aGPS data for GPS chip](HUAMI-GPS)
* [Alarms](Huami-Alarms)
* [Gadgetbridge Configuration](Configuration#mi-band--amazfit-specific-settings)
* [Weather information](Weather)
* [Activity List](Activity-List)
* [Sports Activities - Workouts](Sports-Activities-Workouts)
* [Custom watchface](Watchfaces)
* [Heartrate measurement](Huami-Heartrate-measurement)
* [Heart Rate 3rd party apps data sharing](Heart-Rate-data-sharing)
* [Device Actions](Huami-Device-Actions)
* [Sleep Detection](Huami-Deep-Sleep-Detection)

Support for the Bip 3 Pro was added based on feedback from #3249, but is not extensively tested.

- Firmware and watchface updates were not tested
