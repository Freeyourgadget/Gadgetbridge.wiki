## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/others/shell-racing/

# Shell Motorsport Racing Supercars

All features of the original app are supported, plus some extras :)

- Normal and Turbo speed modes
- Lights On/Off
- Blinking lights
- Predefined tricks (circles and u-turns)
- Battery level
- Battery charging/discharging history

## Joystick control:

<img src="./images/supercars.jpg" width="300" alt="SuperCars Control activity screen with joystick" />
<img src="./images/supercars_battery.jpg" width="300" alt="SuperCars Battery activity with history" />

## Developers:

- Wireshark dissector for the protocol is 
[here](https://codeberg.org/Freeyourgadget/Gadgetbridge-tools/src/branch/main/supercars/wireshark).
- Protocol description and encryption, BLE module description
[here](https://gist.github.com/scrool/e79d6a4cb50c26499746f4fe473b3768)
