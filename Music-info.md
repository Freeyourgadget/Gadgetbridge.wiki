## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/basics/features/music/

# Music info and control

Some bands/watches have support for music. This can vary in features, like:

- controlling playback
    - playback toggle (play/stop)
    - forward/backwards (fast forward/rewind)
- control volume
- receive track info (name)

<img src="images/music_info.png" alt="music info">

## Settings
For basic functionality (playback start/stop) you typically, you do not need to do much settings, but you can set preferred music player in Settings → Prefered Audioplayer. 

## Notification access

For advanced functions like Track info or Volume control, you have to make sure that you put your audio player into list of applications allowed to send notifications.

## Debug

To test if the music info is working, you can use Debug → Set Music Info.

<img src="images/set_music_info.png" alt="set music info">
