## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/internals/topics/support/

### For users or device support enthusiasts

In theory we want to support all good and wide-spread devices.

In practice, someone needs to have the time and the information to implement support for it. If the communication protocol is open, or at least documented somewhere, adding support is rather easy. Ideally, devices would even use standard bluetooth services that we could support in a generic way, with few device specific additions.

We do encourage and support the implementation for new devices. 

When requesting support for a new device, please add the label `device request` to it. If you found documentation or even a specification about the protocol, please reference it. Such documentation very much increases the probability of supporting the device in question with Gadgetbridge.

If you want to learn how to add support for a new device into Gadgetbridge, head over to https://codeberg.org/Freeyourgadget/Gadgetbridge/wiki/New-Device-Tutorial

### For device or firmware makers

If you are looking for an existing "standard Gadgetbridge protocol", to be used inside your gadget, it doesn't exist. For some inspiration, you can look for example at the implementation of [Bangle.js](https://codeberg.org/Freeyourgadget/Gadgetbridge/src/branch/master/app/src/main/java/nodomain/freeyourgadget/gadgetbridge/service/devices/banglejs/BangleJSDeviceSupport.java). Even better, first study the official [Bluetooth GATT services](https://www.bluetooth.com/specifications/gatt/services/) specification. Some of these services are automatically detected by phones and other devices, and might not require a companion app, for example in order to display things like battery status or push music metadata. Then, follow the https://codeberg.org/Freeyourgadget/Gadgetbridge/wiki/New-Device-Tutorial

