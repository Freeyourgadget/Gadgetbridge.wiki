---
gitea: none
include_toc: true
---

## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/headphones/sony/


# Supported features
Note: actual available features per device depends on the device capabilities.

- Ambient Sound Control
    - Off
    - Noise Cancelling
    - Wind Reduction
    - Ambient Sound
        - Volume level
        - Focus on Voice
- Noise Canceling Optimizer
    - Trigger
    - Read atmospheric pressure
- Sound Position Control
- Surround Presets
- Equalizer
    - Presets
    - Custom Presets 1/2 (Bands + Bass)
    - Manual Preset (Bands + Bass)
- Audio Upsampling (DSEE HX/Extreme)
- Touch sensor control panel
- Button modes (left/right touch sensors)
- Ambient Sound Control Button Mode
- Manual Power Off from Gadgetbridge
- Automatic Power Off
- Pause when taken off
- Notification & Voice Guide On/Off
- Quick Access (double / triple tap)
- Speak-to-chat
- Read information from device
    - All User Settings
    - Firmware version
    - Audio Codec
    - Battery level Battery level (Single battery, dual battery, case)
- Disable equalizer / sound position / surround mode when not in SBC codec

# Supported devices

## Sony WH-1000XM2

Firmwares Tested: 4.5.2

### Missing Features

Unknown

## Sony WH-1000XM3

Firmwares Tested: 4.5.2

### Missing Features

- Sound Quality Mode (SBC Codec / HD codecs)
- Function of NC/AMBIENT button
- Notification & Voice Guide Language (needs the language files to be uploaded to the headphones on change)
- Firmware updates

## Sony WH-1000XM4

Firmwares Tested: Based on dumps from 2.5.0, not extensively tested

### Missing Features

- Power Off from app
- Function of \[CUSTOM\] button
- Connect to two devices simultaneously
- Speak-to-cheat settings (only enable/disable implemented)
- Notification & Voice Guide Language (needs the language files to be uploaded to the headphones on change)
- Firmware updates

## Sony WF-SP800N

Firmwares Tested: 1.1.0

### Missing Features

- Trigger Google Assistant / Alexa
- Notification & Voice Guide Language (needs the language files to be uploaded to the headphones on change)
- Firmware updates

## Sony WF-1000XM3

Firmwares Tested: 3.1.2

### Missing Features

Unknown

## Sony WF-1000XM4

Firmwares Tested: 1.4.2

### Missing Features

- Equalizer custom bands
- Voice assistant
- Determine optimal wearing conditions (seal test)
- Speak-to-Chat

## Sony WF-1000XM5

**Experimental device**

Request issue: #3341

### Known issues
- Ambient Sound Control off setting does not work
- Volume Control from earbuds does not work

## Sony LinkBuds S

Firmwares Tested: 2.0.2

### Missing Features

- Multi-point bluetooth (https://codeberg.org/Freeyourgadget/Gadgetbridge/issues/2990#issuecomment-749655)
- Adaptive noise cancelling
- Power off from app (that power button in the bar)

## Sony WH-1000XM5

Request issue: #2969

Firmwares Tested: 1.1.3

### Missing Features

- Multi-point bluetooth
- Adaptive noise cancelling
- Power off from app (that power button in the bar)
- Audio Upsampling
- Ambient sound control button mode
- Touch sensor enable/disable
- React to spotify trigger
- Voice assistant