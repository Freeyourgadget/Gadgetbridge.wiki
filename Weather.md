---
gitea: none
include_toc: true
---

## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/basics/features/weather/


Some bands ([[Pebble]], [[Amazfit Bip]], [[Mi Band 3]], [[Mi Band 4]]) support a weather function to show current weather/temperature at your location and forecast for the following days. Gadgetbridge offers support for forwarding weather information to the band/watch without requiring the INTERNET permission for itself and it needs the help of a *weather provider* on your Android device.

## Weather providers
Most commonly, you need to install and configure a weather provider app, some options are:
* `Tiny Weather Forecast Germany`, instructions [here](#install-and-configure-tiny-weather-forecast-germany).
* `Weather notification`, instructions [here](#install-and-configure-weather-notification). 
* `QuickWeather`, instructions [here](#install-and-configure-quickweather). 
* `Breezy Weather`, instructions [here](#install-and-configure-breezy-weather). 

If you are using CyanogenMod/LineageOS ROM, see instructions [here](#install-and-configure-openweatherprovider). 

## Install and configure "Tiny Weather Forecast Germany"
- It does provide also non-German data, just use city/town names in German or a GPS to determine location, see details below 
- Install  Tiny Weather Forecast Germany [link on F-droid website](https://f-droid.org/packages/de.kaffeemitkoffein.tinyweatherforecastgermany/). 
- After installing, you must enable the Gadgetbridge support in the `Tiny Weather Forecast Germany` app → start the app, Settings → Gadgetbridge support → Enable.
- Set the location as desired and feel free to adjust the other settings. As city names are in German language, best is to use the "Find by location" and either write GPS coordinates or enable GPS permission (even just temporarily), and let the app to determine closest meteo stations.


## Install and configure "Weather notification"
- Install  Weather notification [link on F-droid website](https://f-droid.org/en/packages/ru.gelin.android.weather.notification/). 
- After installing, you must enable the Gadgetbridge skin in the "Weather notification" app → start the app, and flip the Gadgetbridge skin switch as shown in the screenshot. 
- Also make sure that you have the "Enable notification" switch enabled. 
- Set the location as desired and feel free to adjust the other settings (some of them apply only for Pebble watches, and don't affect the Amazefit Bip/MiBands).
- If you use dynamic location (from GPS) make sure Wether Notification app has access to your location.
- You will also need to add an [OpenWeatherMap](https://home.openweathermap.org) API key.

<img src="images/weather_notification_settings.png" width="300" alt="weather notification settings"/>

<img src="images/weather_location_permission.png" width="300" alt="weather location permission"/>

### Summary

* Keep Weather Location settings (CM/LOS) in Gadgetbridge empty
* Install Weather Notification app
* Configure Weather Notifications app:
  * Create and add you OpenWeatherMap API key
      * Create an account at https://openweathermap.org/api
      * Confirm you account creation
      * Wait for the email containing your API key or get it here https://home.openweathermap.org/api_keys
      * Copy the key and paste it into Weather Notifications → OpenWeatherMap → API Key
  * Set location via GPS or manually
  * Enable Gadgetbridge skin
  * Enable notifications
* Open up top notification on phone, select Weather Notification
* Refresh the weather
* Check on your device: weather should be shown


## Install and configure OpenWeatherProvider

The following is for LineageOS 16 or older. For 18.1, "Weather Notification" still works with a slight change in setup. Before adding your OpenWeatherMap api key, add your location manually. Then add the api key and the weather should update.

To use the weather function in LineageOS/CyanogenMod, simply install the OpenWeatherProvider (already installed on CM/LOS 14 and older) from https://download.lineageos.org/extras and configure the cLock widget.

To configure the cLock widget you must register and get the API key from https://openweathermap.org/. 

Set your API key under: *Apps->Clock->Widget Settings->Weatherpanel->Weather source->OpenWeatherMap* 
and also set location either from GPS, or as custom location under:  *Apps->Clock->Widget Settings->Weatherpanel->Use custom location*)

Then, finally also set a location (does need to be the same as above) in the Gadgetbridge weather settings. The location set in cLock and Gadgetbridge are independent, we cannot query the location from cLock.


## Install and configure QuickWeather

- Install QuickWeather [link on F-droid website](https://f-droid.org/packages/com.ominous.quickweather/).
- You will need to add an [OpenWeatherMap](https://home.openweathermap.org) API key in the Settings, during setup.
- In the settings, enable the Gadgetbridge extension. 

## Install and configure Breezy Weather

Breezy Weather

- Install Breezy Weather - [link to the repository](https://github.com/breezy-weather/breezy-weather).
- In Settings -> Widgets -> Scroll down -> enable Send data to Gadgetbridge

## Celsius/Fahrenheit settings

The Settings → Language and region settings → Units → Metric/Imperial is used for weather on some bands (Humai, Pebble and some others).

## Sunrise/Sunset

Some devices provide sunrise/sunset information, you can set your Latitude/Longitude (this is  (wrongly, for historical reasons) in Gadgetbridge, in the Settings → Pebble settings → Location.

## Weather app development

Gadgetbridge weather support works as follows:

- External weather apps send an intent with the weather data, which gets forwarded to the conected device
- Gadgetbridge caches that weather data, for devices that request it themselves (eg. Zepp OS).

There are 5 receivers for weather data:
- WeatherNotificationReceiver (for the Weather Notification app)
- TinyWeatherForecastGermanyReceiver (for the Tiny Weather Forecast Germany app)
- CMWeatherReceiver (for CyanogenMod)
- LineageOsWeatherReceiver (for LineageOS)
- GenericWeatherReceiver (originally for QuickWeather, but can be used by any app)

While apps can use any of the receivers, as long as they conform to the format they expect, new apps integrating with Gadgetbridge should use the `GenericWeatherReceiver`, which was added in #2830. This receiver expects an intent with a json object matching the [WeatherSpec](https://codeberg.org/Freeyourgadget/Gadgetbridge/src/branch/master/app/src/main/java/nodomain/freeyourgadget/gadgetbridge/model/WeatherSpec.java) class fields.

- Action: `nodomain.freeyourgadget.gadgetbridge.ACTION_GENERIC_WEATHER`
- Field: `WeatherJson`

For example:

```
adb shell am broadcast -a "nodomain.freeyourgadget.gadgetbridge.ACTION_GENERIC_WEATHER" --es WeatherJson "{'timestamp':1664117628,'location':'Atlanta','currentTemp':291,'todayMinTemp':291,'todayMaxTemp':301,'currentCondition':'broken clouds, Rain in 25 mins','currentConditionCode':803,'currentHumidity':79,'windSpeed':5.75,'windDirection':290,'forecasts':[{'conditionCode':501,'humidity':68,'maxTemp':301,'minTemp':291},{'conditionCode':800,'humidity':25,'maxTemp':300,'minTemp':290},{'conditionCode':800,'humidity':15,'maxTemp':298,'minTemp':288},{'conditionCode':804,'humidity':21,'maxTemp':295,'minTemp':287},{'conditionCode':804,'humidity':36,'maxTemp':292,'minTemp':287},{'conditionCode':502,'humidity':93,'maxTemp':286,'minTemp':284},{'conditionCode':501,'humidity':96,'maxTemp':288,'minTemp':285},{'conditionCode':803,'humidity':54,'maxTemp':297,'minTemp':286}]}" nodomain.freeyourgadget.gadgetbridge
```

The QuickWeather implementation for this can be found here: https://github.com/TylerWilliamson/QuickWeather/blob/537dc08085cac91e68969a2b98fc5f4492098bcd/app/src/main/java/com/ominous/quickweather/api/Gadgetbridge.java

It's possible to validate / debug the weather data that Gadgetbridge received, by clicking the "Show Cached Weather" button in the debug menu (added in #3077). This button only works for receivers that are present in the manifest, such as the `GenericWeatherReceiver`. Other receivers are only started if a device that supports weather is connected, and will not work otherwise.