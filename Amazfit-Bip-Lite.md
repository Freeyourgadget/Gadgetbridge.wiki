## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/wearables/amazfit/#bip-lite

Contents
* [[Pairing]]
* [Gadgetbridge Configuration](Configuration#mi-band--amazfit-specific-settings)
* [Weather information](Weather)
* [Alarms](Huami-Alarms)
* [Sports Activities - Workouts](Sports-Activities-Workouts)
* [Custom watchface](Watchfaces)
* [Heartrate measurement](Huami-Heartrate-measurement)
* [Heart Rate 3rd party apps data sharing](Heart-Rate-data-sharing)
* [Button Actions](Huami-Button-Actions)
* [Sleep Detection](Huami-Deep-Sleep-Detection)
