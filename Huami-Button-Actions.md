## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/internals/automations/intents-gadget/?h=intents#huami-gadgets-with-a-button

## Configurable button events

Some devices offer a button press to be recognized in the Gadgetbridge and allow assigning actions to these presses.
Currently, this is available in MiBand 2, Amazfit Bip and Amazfit Bip Lite. (know about more? Please help with edits).

You can configure Gadgetbridge to trigger an action or send a broadcast message with a variable `button_id, which is a number indicating an id of the triggered event.


<img src="images/button_actions.jpg" alt="button actions" width="200">

### Gadgetbridge configuration:

* In `Device specific settings` go to `Button actions`.
* Set Button actions to Enabled
* Button press count - set to your desired number of consequent presses to form an `Event`, for example **1**
* Maximum delay between presses - time (ms) within which button presses are joined together into an `Event`, for example **500**

* If you set Button press count to **1**, to trigger Event 1, you must pres **1x**, to trigger Event2, you must press **2x** quickly...

* If you set Button press count to **2**, to trigger Event 1, you must pres **2x** quickly, to trigger Event2, you must press **4x** quickly...

#### Event action

For each Event, you can choose an action. These include *Media Play/Pause*, *Media Toggle Playback*, *Next/Previous Track*, *Volume Up/Down*, *Skup forward/back* and *Send broadcast*. 

#### Broadcast message 

This message can be customized and it also contains a variable called `button_id`, with and ID of the Event.


### Easer

To make use of these broadcast messages, you need to now catch the sent broadcast message and trigger an action. You need an app to do this, you can install [Easer](https://search.f-droid.org/?q=easer), or Tasker...

### Easer configuration

This example shows how to make play/pause toggle to the broadcast `nodomain.freeyourgadget.gadgetbridge.mibandButtonPressed` without utilizing the `button_id` /at this point, Easer actually does not allow utilizing of message parameters, like `device_id`).

* Allow permissions
* Create configuration via Menu → Data
* Profile → + → Title: Toggle → Select an Operation → Android → Media Control → Toggle Play/Pause
* Event → + → Title: Broadcast → Choose an Event → Android → Receive Broadcast → Action: `nodomain.freeyourgadget.gadgetbridge.mibandButtonPressed`
* Condition → Leave empty
* Script → + 
    * Profile: select Toggle
    * Event: select Broadcast

* _Profit ;)_

**Note:** "Toggle Play/Pause" via Easer will hang up a phone call.


### Tasker example:

[Device actions Tasker example](Huami-Device-Actions#user-content-tasker-example-configuration)


