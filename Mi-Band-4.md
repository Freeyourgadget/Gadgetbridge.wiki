## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/wearables/xiaomi/#mi-band-4

### Contents

**WARNING:** The Mi Band 4C is a completely different device, and is **NOT** supported: #2020

* [[Pairing]]
* [Gadgetbridge Configuration](Configuration#mi-band--amazfit-specific-settings)
* [Weather information](Weather)
* [Alarms](Huami-Alarms)
* [[Mi Band 4 Firmware Update]]
* [Custom watchface](Watchfaces)
* [Starting Activity from the band](Sports-Activities-Workouts#user-content-bands-without-integrated-gps)
* [Heartrate measurement](Huami-Heartrate-measurement)
* [Heart Rate 3rd party apps data sharing](Heart-Rate-data-sharing)
* [Sleep Detection](Huami-Deep-Sleep-Detection)

