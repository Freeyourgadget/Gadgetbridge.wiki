## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/wearables/xiaomi/#mi-band-7

Also known as Xiaomi Smart Band 7.

**WARNING:** The Mi Band 7 Pro is a completely different device, and is **NOT** supported: #2781

See the [Zepp OS](Zepp-OS) page for a list of features and issues common to Zepp OS devices.

### Tested Firmwares

- 1.19.1.5
- 1.20.3.1
- 1.27.0.4
- 2.0.0.2

### Tested Hardware

- 0.91.177.3
