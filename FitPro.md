---
gitea: none
include_toc: true
---

## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/wearables/fitpro/

# FitPro devices (devices that use FitPro app)

The band used for the implementation was called `M6` (software version was 0.0.1 and no way to update) but very likely all bands as supported by the same protocol (by the FitPro app) will work. For this, their name will need to be added to Gadgetbridge. Currently `M6`, `M4`, `LH716`, `Sunset 6`, `Watch7` and `Fit1900` have been added. If you have a band which works with the FitPro app and Gadgetbridge discovers it as "Unsupported", you can try to use the "[Add unsupported device](Home#testing-an-unsupported-device)" and choose FitPro as the type of band. Then check if features work OK and report the device in the issue tracker, so we can add it to the list of supported names.

This is an OK device for notifications, weather and occasional music control. The display is not "full screen" as claimed and it only has one button and no touch screen so the menu control is based on short and long taps. Steps counting massively adds steps, so one should not rely on it too much. Same with the sleep and heart data - the measurements are super... fun. Battery seems to last for between three to five days.


## Pairing, connection, reconnecting and general device "security"

When pairing and connecting, is always seems important to press the button on the band to "wake it up" otherwise this can take a long time.

Pairing with the band does not use any key and does not requre user confirmation on the band. Besided this, there is no protection and no secret/random generated code that would be used for subsequent connections, meaning that anyone at anytime (when the band is not connected to a phone) can connect to the band and fetch data. During the "Connecting new device" in Gadgetbridge, the user is asked by Gadgetbridge whether to "pair" or not. This question is related Android pairing. It is ussually better to select the "Pair" as it allows Gadgetbridge to re-connect in a better way, but during them implementation to Gadgetbridge, one of my devices did not work when it was paired... so i have decided to let the user to choose for now, perhaps over time this can be confirmed and set to "Pair" without asking.

The "reconnect" parameter in the device coordinator is set to true, to try to connect to the band in situations that for example notification has arrived and the band is not connected. Overall, the bluetooth range of the band is quite small so disconnections are frequent.

## Implemented Features:

- Discovery, pairing and initialization is implemented, of course
- Set date and time
- Get device info and battery info. On the fly battery notification doesn't seem to be supported by the band. Also charging indication is not available from the band.
- Messages, calls and app notifications with some icons. It seems to support UTF characters.
- Find band, find phone. The `find band` vibration is very short, but if you first request heart rate measurement and immediatelly after you use the "Find lost device", the band will keep vibrating indefinitively. You can stop it with the "Found it".
- Reset (resets settings and steps) and unbind (same as reset plus says "unbinding") band (via Gb Debug reboot and reset)
- Alarms - 8 slots. Quick alarm settings from the [[Widget]] is supported and overwrites the alarm slot number 8.
- Weather with icons
- Media button events, mapped to `previous`, `pause` and `next` music controls.
- Camera buttons events are received but not handled further. In the future, these could be mapped to some custom actions, like take a photo, if we have support for it... This type of control seems to allow to prepare for a shot and then it fires with a wrist turn.
- Heart rate fetching - fetches heartrate, pressure and SpO2. These values are just a joke... but they do get stored to the database. Only HR can be seen in the charts.
<!-- removed - Continuous heart rate - this setting doesn't seem to work on any of my bands but worked on a device with very similar protocol, so i kept this in. -->
- Fetch "day summary" step/distance/calories statistics (this is not per minute data) - is implemented in the code but the data is not used anywhere.
- Steps counting - the band provides steps in 5 minute chunks. This means that often after data fetching, few of the most recent data "seems missing" (the last few, maximum up to five minutes) The original app shows the "day summary steps" to the user to get around that. Gadgetbridge calculates real steps from the real provided activity data. Steps, distance and calories are fetched, only steps are presented in the charts.
- Sleep data - the band provides sleep (light sleep, deep sleep, awake) in 15 minute chunks.


## Implemented Settings:

- [x] Pairing with asking the user (of my two versions, each behave differently, see above)
- [x] User settings + step goal (weight, height, gender, goal and measurement unit (metric/imperial)
- [x] Activate display on lift
- [x] Inactivity warnings
- [x] Do not disturb
- [x] Sleep times
- [x] Continuous heart rate settings (doesn't seem to work on my bands, see above)
- [x] Wearing on left/right arm
- [x] Vibrations on/off
- [x] Notifications on/off
- [x] Language settings
- [x] Alarms + one-shot alarm from the widget (one shot overwrites the 8th alarm)

## Not implemented:

- [ ] Software update
- [ ] Watchface management
- [ ] Remote camera trigger


## Communication protocol

```
cd 00 2a 05 01 0c 00 0c 0000012d000000bb000018
|  |  |  |  |  |  |  |  |-- payload ->
|  |  |  |  |  |  |  |----- payload length low
|  |  |  |  |  |  |-------- payload length high
|  |  |  |  |  |----------- command
|  |  |  |  |-------------- delimiter/version
|  |  |  |----------------- command group
|  |  |-------------------- full length low
|  |----------------------- full length high
|-------------------------- header
```

The protocol seems to be a variation on a frequently used style of communication as seen in different bands inside Gadgetbridge.

- Wireshark dissector and a colorscheme for the FitPro communication protocol, available in the [Gadgetbridge-tools repo](https://codeberg.org/Freeyourgadget/Gadgetbridge-tools/src/branch/main/fitpro).
- Frida script to list classes and get console log (could also be retrieved simply via adb) of 
the FitPro app, available in the [Gadgetbridge-tools repo](https://codeberg.org/Freeyourgadget/Gadgetbridge-tools/src/branch/main/fitpro).
 - I have also found [this project](https://github.com/vanous/MFitX/blob/AntiGoogle/app/src/main/java/anonymouls/dev/mgcex/app/backend/LM517CommandInterpreter.kt) which seem to already fetch some stuff and do other things.

### MTU

Some bands do require MTU size of 20. Other bands do not require it but work OK with this value.

