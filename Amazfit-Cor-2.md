## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/wearables/amazfit/#cor-2

## Amazfit Cor 2 status

This device is very similar to the [[Amazfit Cor]]. We do not have the device at it is largely untested.


### Amazfit Cor 2 firmware

Installation instructions and firmware information are available here: [[Amazfit Cor 2 Firmware Update]]


Please see wiki pages for other Amazfit devices (mainly [Amazfit Bip](Amazfit-Bip)) and link relevant articles here if they apply to this device, much appreciated!