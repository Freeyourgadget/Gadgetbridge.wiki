## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/basics/features/activities/#deep-sleep-detection

### Deep Sleep detection on Huami devices

There is no good Deep Sleep detection on Huami devices. This is an issue currently in ALL Huami devices, with the exception of VERY EARLY factory version of the Amazfit Bip and older Mi Band 2 firmwares.

The reason is that the devices no longer properly distinguish between light and deep sleep but set some *flags* or *hints* for later analysis which is probably done in Mi Fit. The presumtion is, that the MiFit app does some postprocessing on the data. Some work to analyze this has already been done, but no implementation has landed in Gadgetbridge. We would welcome a contribution in this direction.

So at the moment, we ignore these flags and hints, completely because despite of some theories and discussion, it is still unclear how all this works. This information still lands in the database, so if we fix the analysis one day it will retroactively work.

Having said that, the `Use heart rate sensor to improve sleep detection` works and you get good view into your HR during sleep:


<img src="sleep_01.png" alt="Sleep example 1" width="350"/>
<img src="sleep_02.jpg" alt="Sleep example 2" width="350"/>
