## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/wearables/amazfit/#active

**WARNING: Experimental device**

Support for the Amazfit Active was added without access to the device. It should work, but was not tested at all. If you confirm it works or face any issues, please open an issue or reach out in Matrix.

See the [Zepp OS](Zepp-OS) page for a list of features and issues common to Zepp OS devices.
