## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/wearables/xiaomi/#mi-band-1

* [Gadgetbridge Configuration](Configuration#mi-band--amazfit-specific-settings)
* [[Mi Band Compatibility Issues]]
* [[Mi Band Firmware Update]]
* [[Mi Band Firmware Information]] - some technical and historical information about firmware versions
* [[Activity Analysis]]