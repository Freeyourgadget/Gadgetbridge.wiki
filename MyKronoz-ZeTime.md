## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/wearables/mykronoz/

## Getting started

When unboxing the ZeTime, it is most probably completely drained and should be charged for a few hours.

When powering on it says you should connect it with the ZeTime app. But it seems it is possible to skip that step. At least we were successful once.

There seem to be different modes the watch can be in:

* A white screen with a barcode and a serial number, not sure how we get into that. It is possible to connect Gadgetbridge in that state but only "find lost device" (vibration) seems to work, no notifications etc.
* An animation which tells you to install the official app. It is not possible to connect Gadgetbridge in that state. But the screen can be skipped. You can try to press the middle button for a long time to get out of that mode and go to the calibration mode. This is what we did, but after having the white screen and connecting once, not sure if this has to be done or not. It if does not work try to enter "Calibration Mode" as described below. We were not able to get into the "install the ZeTime App" mode again after having done the calibraton, so we cannot re-confirm.
* Calibration mode. At least after reaching the normal operation mode this can be triggered at any time by long pressing the upper and lower buttons till the watch turns off and then pressing the same combination again for a very long time (the ZeTime logo will apper, wait and still hold the button until the animation is interruppted and the screen turns black, then quickly release the buttons)
* Normal operation. This is automatically reached after calibration has been done.

## Known Firmware version

Firmware update is not supported in Gadgetbridge. The official App does not ship firmwares. They are updated via OTA updates.

The following firmware versions are known to us:

|Firmware Version         |Notes
|-------------------------|--------------------------------------------------------
|N1.0A1.0R1.4T0.3H0.5B0.0 |Got this on the watch preinstalled
|N1.0A1.0R2.5T0.3H0.5B3.5 |Mapping of weather infomation needs Gadgetbridge >=0.44.0

## Userland

See the support request to find out more about the beginning -> [Issue 1099](https://codeberg.org/Freeyourgadget/Gadgetbridge/issues/1099).

Initial [announcement](https://github.com/Freeyourgadget/Gadgetbridge/pull/1148) of support for ZeTime in Gadgetbridge.

Find a forum to discuss the actual state of support -> [xda-developers.com](https://forum.xda-developers.com/android/apps-games/testing-gadgetbridge-zetime-support-t3813241)

## Tech Docs

Find here an analysis of the [ZeTime Protocol](https://codeberg.org/Freeyourgadget/Gadgetbridge/wiki/ZeTime-Protocol-Analysis) made by [Sauce Maison](https://github.com/SMaiz), commented and a little bit extended by [Sebastian Kranz](https://github.com/lightwars).

For further "Header Codes" of the protocol see the sources [here](https://codeberg.org/Freeyourgadget/Gadgetbridge/src/branch/master/app/src/main/java/nodomain/freeyourgadget/gadgetbridge/devices/zetime/ZeTimeConstants.java).
