## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/wearables/amazfit/#band-7

See the [Zepp OS](Zepp-OS) page for a list of features and issues common to Zepp OS devices.

### Tested Firmwares

- 1.10.31.1

### Tested Hardware

- 0.99.21.5
