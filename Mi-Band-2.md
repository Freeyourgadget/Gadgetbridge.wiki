## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/wearables/xiaomi/#mi-band-2

* [Gadgetbridge Configuration](Configuration#mi-band--amazfit-specific-settings)
* [[Pairing]]
* [[Mi Band 2 Firmware Update]]
* [[Mi Band 2 HMZK Font Format]]
* [Heartrate measurement](Huami-Heartrate-measurement)
* [Heart Rate 3rd party apps data sharing](Heart-Rate-data-sharing)
* [Button Actions](Huami-Button-Actions)
* [Sleep Detection](Huami-Deep-Sleep-Detection)

## Fonts

To display text notifications, you might need to install a font, as those are sometimes not installed on the device by default.

This process is identical to the *Mi Band 2 Firmware Update* process above. After extracting the APK, select the `Mili_pro.ft.en` file instead.


Some users reported that after initial adding to gadgetbridge, using the `Disable bluetooth pairing` option was important to actually allow the connection to happen.