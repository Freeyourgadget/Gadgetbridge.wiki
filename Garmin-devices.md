Support for the first Garmin device was added in the summer of 2023 in [this pull request](https://codeberg.org/Freeyourgadget/Gadgetbridge/pulls/3180).

# The support for Garmin devices is in the codebase but has not been published yet on F-droid.

## Garmin protocols
There are (at least) two big families of garmin protocol: both are using BLE. The first protocol uses different characteristics to transfer different type of information and appears to be in use on older wearables: the aforementioned contribution made Gadgetbridge support this version of the protocol.

A newer protocol uses the same characteristics for every information transfer, by encapsulating this in "threads" (or handles, or conversations) whose identifiers are prepended to each message exchange. This version of the protocol is currently unsupported.

A third version of the protocol was also [reported](https://codeberg.org/Freeyourgadget/Gadgetbridge/issues/3063) that apparently adds some kind of sequence between the thread/handle/conversation ID and the actual payload, while using the same characteristic.


The difference between the first protocol and the rest can be detected early by the presence of the specific characteristic used in newer protocols.
The second and the third protocol might probably be chosen according to some early handshake values and the way some identifiers are chosen by the watch, so it should be possible to support all three generations of the protocols with relatively little effort.


## Supported features

For the moment, please see the PR linked above to check which features are supported. Some feature are implemented but their support should be considered experimental for the time being.

## Supported devices
The supported device list is:

- Vívomove HR

It is likely that other devices of the same generation could be added with a relatively minor effort.

Additionally, work is in progress to add support for newer devices like ``Vívomove Style`` and ``Forerunner 245``. ``Instinct solar`` is also [reported](https://codeberg.org/Freeyourgadget/Gadgetbridge/issues/2280#issuecomment-1278859) to be compatible with this work in progress as well as ``Vivoactive 4`` [issue](https://codeberg.org/Freeyourgadget/Gadgetbridge/issues/3299).

Devices that need the third version of the protocol are: ``Instinct Crossover`` [reported here](https://codeberg.org/Freeyourgadget/Gadgetbridge/issues/3252) and ``Instinct 2`` [issue](https://codeberg.org/Freeyourgadget/Gadgetbridge/issues/3063). 

However our analysis indicates that there is no direct correlation between watch models and protocol version, as **the protocol used by a specific device might change with the firmware** version it runs. 

## Additional information

- We have noticed that in some cases the bluetooth traffic contains some bits of information (``oauth_token``, ``oauth_consumer_key``, ``oauth_signature``, ...) that might (potentially, we did not test) grant access to the user's garmin account. **Therefore we warn all the users to mind this information when sharing the logs.**

- User data/configuration is unmanaged, which means that whatever was set in the official app will remain untouched, no matter what is set in Gadgetbridge