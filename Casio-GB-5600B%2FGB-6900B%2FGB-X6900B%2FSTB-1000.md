## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/internals/specifics/casio-protocol/

The Casio GB-5600B/GB-6900B/GB-X6900B/STB-1000 watches are one of the first Casio models with Bluetooth and are the first Casio models supported by Gadgetbridge. Unfortunately, the BLE implementation on the watch is buggy so your mileage may vary.

# Connecting

Connecting to the watch sometimes takes more than one try. If you enable automatic BLE reconnect, the process might be automatic. When connecting for the first time, be sure to delete the existing pairing from both, the phone and the watch, before adding it to Gadgetbridge.

**NB**: The very first connection, i.e. when pairing, does not provide any functionality. You have to manually disconnect and reconnect the watch to make it fully working. To be sure, disable and enable Bluetooth on the phone between the connection attempts in order to refresh the GATT characteristic cache.

# Automatic Reconnect
Casio watches tend to lose connection from time to time, especially when using LTE data very heavily. While the automatic BLE reconnect included in Gadgetbridge sometimes works, it is often not fast enough to find the Casio watch, since it advertises only for about 8 seconds before turning Bluetooth off. A Pull Request ([PR 1416](https://github.com/Freeyourgadget/Gadgetbridge/pull/1416)) to improve reconnection is active but has not yet been merged. This situation has improved significantly and the PR is abandoned.

# Battery Optimizations
If you experience delays with receiving notifications, try to disable battery optimizations for Gadgetbridge **and report your problem to  [Issue 1439](https://codeberg.org/Freeyourgadget/Gadgetbridge/issues/1439)**.

# Implemented features

* Notifications
* Music Control
* Phone Finder
* Time synchronization
* Alarm Configuration

# Missing features

Currently, the only adjustments to the watch are setting alarms and time synchronization. No further configuration is possible.

# Model-specific details

## STB-1000

Control mode is active directly after connection has been established. In order to leave this mode, long-press the MODE button on the watch.

# Implementation specific details

The watch seems to always require time synchronization on connecting. Thus, the respective option in Gadgetbridge's settings has no effect for Casio models, the time is always synchronized when requested by the watch.

# Protocol and other models

The implementation for Gadgetbridge is based on Bluewatcher (https://github.com/masterjc/bluewatcher) and additional analysis work. There is no formal specification of the protocol available.
