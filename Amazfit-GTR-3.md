## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/wearables/amazfit/#gtr-3

See the [Zepp OS](Zepp-OS) page for a list of features and issues common to Zepp OS devices.

### Tested Firmwares

- 7.46.1.1

### Tested Hardware

- 0.84.19.19
