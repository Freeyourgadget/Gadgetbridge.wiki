## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: This wiki has been deprecated and replaced by the new website: https://gadgetbridge.org/

### FAQ

For Frequently asked questions, see our [[FAQ]].

### Obtaining Gadgetbridge
There are multiple ways to get Gadgetbridge:

* Official release channel is done via [F-Droid](https://f-droid.org/en/), see listing for [Gadgetbridge](https://f-droid.org/packages/nodomain.freeyourgadget.gadgetbridge) and we encourage you to install the app through there. This will ensure timely updates and signed packages which are verifiably built from the published source code.

* If you do not have F-Droid installed/cannot install it, you can download official, signed Gadgetbridge apks from the [Gadgetbridge](https://f-droid.org/packages/nodomain.freeyourgadget.gadgetbridge) listing page and side-load them into your device. You then have to take care of updating yourself.

* Use Gadgetbridge F-droid repository of Nightly releases, see more info [here](FAQ#is-there-a-way-to-get-more-frequent-updates-of-gadgetbridge-like-a-nightly-release)

* You can build Gadgetbridge from source code.


### Conflict with Pebble apps

If you get the following error: `Failure [INSTALL_FAILED_CONFLICTING_PROVIDER: Scanning Failed.: Can't install because provider name com.getpebble.android.provider (in package nodomain.freeyourgadget.gadgetbridge) is already used by com.getpebble.android]`, this is not a bug, it is a platform restriction that can not be lifted. Please choose either one of the apps.

### Pairing/Connecting new device

* It is typically NOT necessary to install the MiFit app at all. Unfortunately, this has been changing over time. New devices and older devices with updated firmware require server based pairing. This means, that you absolutely must use MiFit/Amazfit/Zepp app to make the initial pairing, retrieve the pairing key and then use this key to pair with Gadgetbridge. Currently, some devices:
   * **Mi Band 4**
   * **Mi Band 5**
   * **Mi Band 6**
   * **Mi Band 7**
   * **Amazfit Bip Lite** 
   * **Amazfit GTR**
   *  **Amazfit GTS**  
     are known to be the first device with this enabled from the beginning. 
     **Read page [[Pairing]] for further details.**
* If you have the MiFit/Amazfit/Zepp app, make sure it is not running. Kill or uninstall it.
* On some devices. you may need to go to the watch settings and click the "Add new phone" button
* Unpair the band/watch from the phone's Bluetooth.
* Ensure GPS/location services are enabled on your device (this is related to Android permission system, since Android 6).
* Make sure to give Location service permissions to Gadgetbridge.
* Run Gadgetbridge and add new device to Gadgetbridge by pressing the Plus button.
* After the device has been discovered, follow the instructions. 
   * **Pebble** Bluetooth Low Energy support needs to be enabled in the settings before discovery, if you wish to use it
   * CompanionDevice API support increases the service's reliability, but it needs to be enabled in the settings before discovery, if you wish to use it
* For further information check out the wiki for details about general configuration, some specifics about your particular device or, FAQ or developer info.


### Testing an unsupported device

Do not perform without being advised.

You can now try if some existing implementation for some device can also control another device which might be of a similar type, rebranded device and so on. Do note, that if the device requires special pairing key (like the Huami devices), you must obtain it. 

* **Read page [[Pairing]] for further details.** 

Other requirements from above are still valid, like...:

* If you have the MiFit/Amazfit/Zepp app, make sure it is not running. Kill or uninstall it.
* Unpair the band/watch from the phone's Bluetooth.
* Ensure GPS/location services are enabled on your device (this is related to Android permission system, since Android 6, read [more about it here](Permissions-Explained).).
* Make sure to give Location service permissions to Gadgetbridge, read [more about it here](Permissions-Explained).

To test unsupported device:

- Run Gadgetbridge and open the Add new device by pressing the Plus button
- In the Discovery and pairing options select "Discover unsupported devices"
- Scan for devices. When your device is found, it has the "Unsupported" label
- Long tap on the device, a dialog will pop-up and here you select a device which you think might be suitable
- If the device requires special pairing key, open the Device preferences and edit the Auth Key
- Tap the Device card to try to connect to the device

