## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/wearables/amazfit/#bip-u

## Amazfit Bip U

* [[Huami Server Pairing]]
* [Firmware Update](Amazfit-Bip-U-Firmware-Update)

The Amazfit Bip U is a device that is supported. But the wiki page for it is not written yet... But the pairing link above will get you started conecting your device.
