## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/wearables/amazfit/#neo

The Amazfit Neo has a simple monochrome LCD and an advertised battery life of ~28 days, which makes it much more like a regular watch than a smartwatch. Nonetheless it has sleep tracking and a heart rate sensor. Unlike some other Amazfit watches it uses a beeper instead of a vibrator.

To pair the watch with Gadgetbridge for the first time, you will need to use the official app and get the authentication key as described in [[Huami Server Pairing]].

### Current status

Most features were added in #2117 but a few features are not supported yet.

Works:
- notifications
- find phone / watch
- set alarm
- live heart rate
- set time format
- most "display items" are displayed
- download/sync activity data
- setting automatic heart rate measurement interval
- synchronizing time
- weather
- enabling/disabling beeps
- inactivity notification

Feedback needed:
- workout screen
- "PAI"

Does not work:
- disconnect notification

### Display items / pages

The watch allows you to cycle through the "display items" or pages using the "up" and "down" buttons.

- PAI
- DND
- heart rate
- weather
- stopwatch
- alarm 
- step count
- "distance"
- "consumption" (calories burned)
- battery level
- world clock

### To Do
- make an icon (using the Amazfit Bip icon currently)
- unlikely: custom firmware + full-text notifications
- testing / feedback

### Feedback

There are still questions about whether some of the features work properly or not. Please give feedback (edit this page, open an issue, etc.), especially if you have used the proprietary app and know how it's supposed to work.

