## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/wearables/amazfit/#gtr

Contents
* [[Pairing]]
* [Alarms](Huami-Alarms)
* [Gadgetbridge Configuration](Configuration#mi-band--amazfit-specific-settings)

Please see wiki pages for other Amazfit devices (mainly [Amazfit Bip](Amazfit-Bip)) and link relevant articles here if they apply to this device, much appreciated!
