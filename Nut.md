## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/internals/specifics/nut/

The devices are very similar to the [iTag series](https://codeberg.org/Freeyourgadget/Gadgetbridge/wiki/iTag). Gadgetbridge does **not support** these devices at the moment. 

There are various models available called "Nut Nutale", "Nut Find3", "Nut Mini"  and  "Nut Smart", there's also probably a "Nut Find2". The devices might also be sold under the names of "Nut Find 3", "Nutale Focus" and "NutMini".

As with the iTag series, the Nut devices function as low-powered BLE beacons with the option to make sound and/or flash lights to locate the thing they're attached to. 

Purchasing of these devices **is not recommended**. They do not use standard methods for providing the needed functionality and Gadgetbridge does not support setting the devices up, the devices require sign-up to use the proprietary app.

There's also the issue of account-based DRM, the devices get locked to a single account and have no official way of reuse. The price is also extraorbitant when iTag devices are 5-10x cheaper.

The OTA firmware images `firmware_sys.hex` or `firmware_sys.zip` use the AES key `16e92d2456e3d7a5`, "standard" Nordic DFU is used for firmware upload.


### Nut Mini 3

Based on the nRF51802 MCU. Requires an authentication key to work. You can get the authentication key by just capturing a hci_dump, when you just connect to the device it will contain only three packets. The first two are the challenge (starts with `0x01`) and the response (begins with `0x02`), third contains `0x03 0x55` that just says the authentication was correct (`0x03 0xaa` means failure).
