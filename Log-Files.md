---
gitea: none
include_toc: true
---

## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/internals/topics/logs/


This article is about application logging in Android which consists of dumps a log of system messages, including stack traces when the device throws an error and messages that have been written by the application. These can be retrieved via [logcat](https://developer.android.com/studio/command-line/logcat.html#startingLogcat). Gadgetbridge also provides a way to save the application log into a file, which can then be read.

## Log File Contents

Please keep in mind Gadgetbridge log files may contain **lots of personal information**, including but not limited to health data, unique identifiers (such as a device MAC address), music preferences, etc. Additionally, consider that if you used your device with the official app in the past, the Bluetooth log might contain information about your account on the vendor's cloud, including ways to access it. Consider editing the file and removing this information before sending the file to a public issue report.

When sharing a log file, please try to make it as short as possible. If you already had logging enabled, disable and reenable it before doing the things that you want to get logged. Disable logging when you're done with that. That way, we will not have to wade through huge log files looking for the actual relevant lines.

## Log File Location

If you enable Gadgetbridge's logging in the "Settings" → "Write Log Files", the log file will be stored in the Export/Import application folder called "files". Location of this application folder depends on Android version but in modern Android, the folder path is `/storage/emulated/0/Android/data/nodomain.freeyourgadget/files/`. Older Android might have this folder on your external sdcard in `/sdcard/Android/data/nodomain.freeyourgadget.gadgetbridge/files/`. The log file is named `gadgetbridge.log`.

## Sharing the log file directly
Gadgetbridge offers the possibility to share the log file directly from the app, reducing the need to use the file manager approach outlined below. Go to "Debug" in the menu and click the "Share log" button, then choose which app and/or contact to share the log with.

## Accessing the Export/Import folder

The [Export/Import folder](#export-import-folder) can be normally accessed via
some file manager. Under Android 11 and newer versions, the Export/Import
folder can only be accessed via file manager that is using the new [Scoped
Storage](https://source.android.com/devices/storage/scoped) system of Android.

One popular FLOSS file manager which is able to do this is a Simple File Manager Pro, link to: [Google
Play](https://play.google.com/store/apps/details?id=com.simplemobiletools.filemanager.pro),
[F-Droid](https://f-droid.org/en/packages/com.simplemobiletools.filemanager.pro/): 

<img src="images/simple_file_manager.jpg" alt="Simple File Manager Pro
screenshot" width="700">
