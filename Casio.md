## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/wearables/casio/

# Supported Watches and Status

Initial support for Casio watches was done on a GB-X6900B here: [PR 1377](https://codeberg.org/Freeyourgadget/Gadgetbridge/pulls/1377). In the meantime, explicit support for the following watches was enabled:

* [Casio GB-X6900B](Casio-GB-5600B%2FGB-6900B%2FGB-X6900B%2FSTB-1000)
* [Casio GB-6900B](Casio-GB-5600B%2FGB-6900B%2FGB-X6900B%2FSTB-1000)
* [Casio GB-5600B](Casio-GB-5600B%2FGB-6900B%2FGB-X6900B%2FSTB-1000)
* [Casio STB-1000](Casio-GB-5600B%2FGB-6900B%2FGB-X6900B%2FSTB-1000)
* [Casio GBX-100](Casio-GBX-100%2FGBD-200)
* [Casio GBD-200](Casio-GBX-100%2FGBD-200)
* [Casio GBD-H1000](Casio-GBX-100%2FGBD-200)
* Casio GW-B5600 (#3218)
* Casio GMW-B5000 (#3218)

Please keep in mind that the different models are supported to varying degree.
