This page holds specifics to the "[Bluetooth Low Energy](https://en.wikipedia.org/wiki/Bluetooth_Low_Energy)" (BTLE) devices of Pebble – meaning all Pebble 2 variants, but also your other Pebble should you use it in that mode (e.g. the Pebble Time Steel also supports BTLE).


Content moved to [[Pebble Pairing]]