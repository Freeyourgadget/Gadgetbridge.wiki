---
gitea: none
include_toc: true
---

## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/basics/features/sports/


## Integrating Sports Tracking apps with Gadgetbridge Sports Activities/Workouts

If you like to make a GPS record of your activity, you can use another sports
tracking app and utilize some dedicated Gadgetbridge features that make
integration nicer. This allows to utilize the Activity tracking on the watch
with some addition details and processing, like number of steps or strokes and
so on.

See a [dedicated
post](https://blog.freeyourgadget.org/add_gps_track_to_any_sports_activity.html)
on our blog. Also see the [[Sports Activities Workouts]] page.

This now offers a new possibility to link a GPX file to a sports activity that
did not even allow to have a GPS recording in the first place - like outdoor
rowing while using the rowing machine recording or pool swimming (if you have
a waterproof phone).

Some activities that you can record without GPS (and make some sense to have
GPS linked) include Workout, Outdoor running, Outdoor walking, Treadmill,
Freestyle, Indoor cycling, Rowing machine, Pool swimming. These of course must
exist in the band/watch, which depends on the watch version.

In this manual, for the tracking i focus on two nice FLOSS sports tracking apps: [OpenTracks](https://f-droid.org/packages/de.dennisguse.opentracks) and [FitoTrack ](https://f-droid.org/packages/de.tadris.fitness) (random order :) ).

### OpenTracks

### Settings

All these setting only need to be done once.

#### Enable heart rate data sharing in Gadgetbridge.

<img src=images/integrating_sports/01prefs.jpg width=220 alt="settings screenshot">
<img src=images/integrating_sports/02.jpg width=220 alt="settings screenshot">

#### Connect your watch/band to OpenTracks.

<img src=images/integrating_sports/07menu.jpg width=180 alt="settings screenshot">
<img src=images/integrating_sports/08settings.jpg width=180 alt="settings screenshot">
<img src=images/integrating_sports/08bluetooth_annotated.jpg width=180 alt="settings screenshot">
<img src=images/integrating_sports/09.jpg width=180 alt="settings screenshot">

#### Set autoexport.

Set OpenTracks to automatically export recordings. This only need to be done once. Set the output format as GPX. Ideally, set the output folder to be the Gadgetbridge [files folder](https://codeberg.org/Freeyourgadget/Gadgetbridge/wiki/Data-Export-Import-Merging-Processing#export-import-folder).

_At them moment, on Android 11 and newer, OpenTracks cannot export directly into Gadgetbridge [files folder](Data-Export-Import-Merging-Processing#user-content-export-import-folder) due to the Scoped Storage permissions, but you can always manually copy the file there, see for example [here](https://codeberg.org/Freeyourgadget/Gadgetbridge/wiki/Data-Export-Import-Merging-Processing#accessing-the-export-import-folder) or you can send it via the Gadgetbridge's GPX Receiver, see below._

<img src=images/integrating_sports/08settings.jpg width=220 alt="settings screenshot">
<img src=images/integrating_sports/11annotated.jpg width=220 alt="settings screenshot">

### Recording activity

#### Start recording

##### start recording an Activity on the band/watch

<img src=images/integrating_sports/treadmill.jpg width=220 alt="settings screenshot">
<img src=images/integrating_sports/recording.jpg width=220 alt="settings screenshot">

##### start recording in the OpenTracks

<img src=images/integrating_sports/07start.jpg width=220 alt="settings screenshot">
<img src=images/integrating_sports/10hr.jpg width=220 alt="settings screenshot">

#### Stop recording

When you are finished, stop recording on the watch and in OpenTracks. 

##### Sync the Sports Activities with Gadgetbridge.

<img src=images/integrating_sports/01workouts.jpg width=220 alt="settings screenshot">
<img src=images/integrating_sports/syncing.jpg width=220 alt="settings screenshot">

### Link recorded GPX with the Gadgetbridge Sports Activity

In Gadgetbridge, open recorded Sports Activity

<img src=images/integrating_sports/01workouts.jpg width=220 alt="settings screenshot">

Edit GPX track

<img src=images/integrating_sports/13editgps.jpg width=220 alt="settings screenshot">

Select the track

<img src=images/integrating_sports/13_select.jpg width=220 alt="settings screenshot">

Confirm

<img src=images/integrating_sports/16ok.jpg width=220 alt="settings screenshot">

GPS track is now added 

<img src=images/integrating_sports/17.jpg width=220 alt="settings screenshot">


### FitoTrack

### Settings

All these setting only need to be done once.

#### Enable heart rate data sharing in Gadgetbridge.

<img src=images/integrating_sports/01prefs.jpg width=220 alt="settings screenshot">
<img src=images/integrating_sports/02.jpg width=220 alt="settings screenshot">

#### Connect your watch/band to FitoTrack.

This is done only once, but tracking recording must be running.

<img src=images/integrating_sports/03b_running.jpg width=180 alt="settings screenshot">
<img src=images/integrating_sports/25menu.jpg width=180 alt="settings screenshot">
<img src=images/integrating_sports/23connect.jpg width=180 alt="settings screenshot">
<img src=images/integrating_sports/24.jpg width=180 alt="settings screenshot">

Stop the recording.

### Recording activity

#### Start recording

##### start recording an Activity on the band/watch

<img src=images/integrating_sports/treadmill.jpg width=220 alt="settings screenshot">
<img src=images/integrating_sports/recording.jpg width=220 alt="settings screenshot">

##### start recording in the FitoTrack

<img src=images/integrating_sports/03b_running.jpg width=220 alt="settings screenshot">
<img src=images/integrating_sports/26hr.jpg width=220 alt="settings screenshot">

#### Stop recording

When you are finished, stop recording on the watch and in FitoTrack. 

#### Sync the Sports Activities with Gadgetbridge.

<img src=images/integrating_sports/01workouts.jpg width=220 alt="settings screenshot">
<img src=images/integrating_sports/syncing.jpg width=220 alt="settings screenshot">

#### Share the GPX track from FitoTrack with Gadgetbridge

In FitoTrack, open the recorded Activity and share it with GPX Receiver Gadgetbridge

<img src=images/integrating_sports/04export.jpg width=220 alt="settings screenshot">
<img src=images/integrating_sports/05share.jpg width=220 alt="settings screenshot">
<img src=images/integrating_sports/06receiver.jpg width=220 alt="settings screenshot">

This will open Gadgetbridge GPX Receiver, confirm OK.

<img src=images/integrating_sports/14ok.jpg width=220 alt="settings screenshot">

Existing files overwriting is also indicated:

<img src=images/integrating_sports/15overwrite.jpg width=220 alt="settings screenshot">


### Link recorded GPX with the Gadgetbridge Sports Activity

In Gadgetbridge, open recorded Sports Activity

<img src=images/integrating_sports/01workouts.jpg width=220 alt="settings screenshot">

Edit GPX track

<img src=images/integrating_sports/13editgps.jpg width=220 alt="settings screenshot">

Select the track

<img src=images/integrating_sports/13_select.jpg width=220 alt="settings screenshot">

Confirm

<img src=images/integrating_sports/16ok.jpg width=220 alt="settings screenshot">

GPS track is now added

<img src=images/integrating_sports/17.jpg width=220 alt="settings screenshot">

### View details of the recorded GPX track

To see the track in more detail, you can of course use the "Show GPS Track" to
export or view the recording in another app - like in OpenTracks, FitoTrack, OsmAnd~, AAT
Another Activity Tracker or others.

- [OpenTracks](https://f-droid.org/packages/de.dennisguse.opentracks)
- [OSM Dashboard for OpenTracks](https://f-droid.org/packages/de.storchp.opentracks.osmplugin/)
- [OSM Dashboard Offline for OpenTracks](https://f-droid.org/packages/de.storchp.opentracks.osmplugin.offline/)
- [FitoTrack ](https://f-droid.org/packages/de.tadris.fitness) (not via sharing icon, but by using import function in FitoTrack)
- [Another Android Tracker](https://f-droid.org/packages/ch.bailu.aat/) (seems like more precise representation of time and distance, calculates calories)
- [osmAnd+](https://f-droid.org/packages/net.osmand.plus/) 
- [Milkha](https://apt.izzysoft.de/fdroid/index/apk/com.tensorfoo.milkha) 

<img src=images/integrating_sports/27showgps.jpg width=220 alt="settings screenshot">
<img src=images/integrating_sports/18.jpg width=220 alt="settings screenshot">
<img src=images/integrating_sports/19.jpg width=220 alt="AAT">

<img src=images/integrating_sports/20.jpg width=220 alt="OsmAnd~">
<img src=images/integrating_sports/21.jpg width=220 alt="OpenTracks">
<img src=images/integrating_sports/22.jpg width=220 alt="OpenTracks">


### Starting the sports app directly from the watch/band

OpenTracks supports an Intent based integration to start/stop workout recording. This is implemented into some devices in Gadgetbridge, read full description (mostly for developers) [here](OpenTracks-API). To use it:

- Enable the API in OpenTracks
- In Gadgetbridge settings, select "OpenTracks package name" based on which
  OpenTracks version you installed. (official, playstore, debug, nightly...)
- Use this function either in:
    - Fossil: 
        - it is implemented as a workout app - when a workout is started, the OpenTracks is started to record the activity.
    - In Miband/Amazfit devices which support [Button](Huami-Button-Actions) and [Device](Huami-Device-Actions) actions:
        - set "Fitness app tracking start/stop/toggle" as one of the Button/Device actions
### Notifications

#### Audio Announcements

Both FitoTrack and OpenTracks allow you to enable audio notifications of the
current progress into your headphones. 

#### Ongoing Notifications

OpenTracks and  FitoTrack also provide an "ongoing notification" in the Android
notifications bar. We have enabled ongoing notifications specifically for these
two apps and this allows you to observe notifications of an ongoing activity
tracked by these apps right on your wrist. 

Normally, we block ongoing notifications because these could be emitted many
times per second and thus cause fast battery drain and also other possible
issues. It is therefore recommended to set the `Settings` → `Minimum time
between notifications` to a few seconds.  You can of course always block
notifications of these apps completely via the regular `Notification
blacklist`. Both of these settings are in Gadgetbridge Settings.
