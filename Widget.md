## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/basics/topics/widgets/

### Gadgetbridge Widgets

There are two widgets in Gadgetbridge:

* [Zzz widget](#zzz-widget) for quick alarm setting
* [Main widget](#main-main) which shows current step count, sleep hours, status and allows alarm settings too

#### Zzz widget

Upon tapping the Zzz, this widget quickly sets an alarm after "Preferred sleep duration" as set in Preferences → About you. This alarm is set by overwriting the first alarm on the device.

#### Main widget

It shows current device name, Total step count, Total distance (steps * step length), sleep hours (since the previous day's noon) and battery status/level (Not connected/Connecting/Battery percentage). 

The widget provides quick access to several actions:

* Tap on the Device name requests data re-sync. If device is currently not connected via Bluetooth, Gadgetbridge attempts to reconnect first
* Clock icon opens Alarm popup to quickly set an alarm for 5/10/20mins/1hour/8hours (last item uses hours value as is set in your Preferences → About you → Preferred sleep duration). This alarm is set by overwriting the first alarm on the device.
* Gadgetbridge icon opens the Gadgetbridge app
* Steps/distance/sleep opens up the Activity charts in Gadgetbridge
* Long tap the widget to resize it


<img src="images/widget/widget_description.png" alt="Widget Actions" >
