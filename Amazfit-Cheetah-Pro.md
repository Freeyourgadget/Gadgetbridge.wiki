## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/wearables/amazfit/#cheetah-pro

Support for the Amazfit Cheetah Pro was added in #3251

See the [Zepp OS](Zepp-OS) page for a list of features and issues common to Zepp OS devices.
