## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/trackers/itag/

The iTag devices are small battery-powered wearable Bluetooth LE beacons intended for link loss monitoring.

There is currently no support for button-based actions, contributions are welcome.

Gadgetbridge supports quite a few different physical devices under the iTag name that use many different chipsets and software versions. Due to that, the stability and behaviour of each and every device can not be guaranteed. **Do not rely on Gadgetbridge and iTag devices for safety purposes!**

It is recommended to pair/bond with the tracker if possible, some of the iTag devices **will drain batteries in hours if you don't**, but some models might crash when you try to do so. It's up to you to find out which sort of device you've stumbled upon. Unfortunately detecting if pairing is supported or necessary can't be done automatically.

If the device reports battery status, Gadgetbridge will try to display it. If you don't see a battery indicator, the device either does not report battery status or does so in a non-standard way.

See also similar [Nut devices](https://codeberg.org/Freeyourgadget/Gadgetbridge/wiki/Nut).


