## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/wearables/amazfit/#gtr-4

See the [Zepp OS](Zepp-OS) page for a list of features and issues common to Zepp OS devices.

### Tested Firmwares

- 3.8.5.1
- 3.17.0.2
- 3.18.1.1

### Tested Hardware

- 0.102.19.1
