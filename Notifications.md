---
gitea: none
include_toc: true
---

## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/basics/features/notifications/


## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/internals/topics/activity-analysis/

Gadgetbridge offers wide notifications possibilities and settings. Some are global, some might be device specific.

## Notification Access on Android 13+

Due to some changes to permissions granted to apps on Android 13+, it might not be possible to grant notification permissions to Gadgetbridge when installing it from F-Droid.

If you see a warning that notification access is a restricted setting, find  Gadgetbridge in the Android settings, and click "Allow restricted settings" in the top right corner menu.

See #3289 for more information.

### Persistent notification in Notifications drawer

There is a persistent notification in the Notifications drawer.

<img src=images/notifications/persistent_notification.jpg width=200>

For the most time, this is actually required, to ensure that Gadgetbridge keeps running in the background and keeps Bluetooth connection with the band/watch (even if the application is not currently open and used), that is required by Android itself. This notification can be hidden via Settings →  "Hide the Gadgetbridge notification". In Android 10, this settings might not be working anymore, but you can hide the notification on Android 10 when you long-press on the notification and hide it from there.

### Phone Calls, SMS, generic notification support

The Settings menu has several items allow to tweak the notifications as required, for example choose whether incoming calls should be signalled to your Gadget, choose when incoming SMS should cause notifications on your Gadget: always, only when your Android device’s screen is off, never... You can also set Minimum time between notifications etc.

<img src=images/notifications/settings_menu.jpg width=200>

### SMS notifications are lost somewhere

#### Problem

You are using alternative SMS app (like Silence or Signal). Stock one is present and not disabled, but the alternative one is set up to be default.
All notifications but SMS are shown. Log says that *Gadgetbridge* is `Ignoring notification, is a system event`

#### Solution

You have to disable SMS support in Gadgetbridge settings to fix your problem (change it to *Never*).

The reason is that we blacklist alternative SMS Apps in the generic notification receiver since it would otherwise result in double notifications (one picked up from the Android SMS receiver, and one picked up from the actual notification)

In case the SMS is encrypted, it would not make sense to use the SMS receiver, but only the notification receiver which already contains the decrypted content.

### Incoming call notification on Lineage OS

#### Problem

Using Gadgetbridge under Lineage OS could prevent the notification of incoming
calls on your Amazfit Bip due to settings in the "privacy guard" of Lineage OS.

#### Solution

Go to "Option" -> "Security" -> "Trust".
Touch the "More Options Icon" and than "Advanced".
Search for Gadgetbridge and click on it.
Set the reading of call log history to "Allowed"

see https://codeberg.org/Freeyourgadget/Gadgetbridge/issues/1477


### Device specific notification settings

Some devices allow you to receive additional notifications, here are few examples:

Notification when you reach your daily goal, like steps etc. Some devices might also offer specific vibrations settings. Disconnect notification, which can be set up also only for some Scheduled time interval period.

### Notification blacklist management

You can adjust very previse notification filtering for each individual application installed on the Android device.

#### App blacklisting

For each app (you can search for a particular app), you can choose if notifications are Disabled (blacklisted) by checking the NOTIF checkbox.

<img src=images/notifications/list.jpg width=200>

You can also set all apps to enabled/disabled in bulk via the dots **⋮** menu.

<img src=images/notifications/bulk_menu.jpg width=200>

#### Detailed per app filtering

For each app, you can establish detailed filtering rules, allowing to forward or block particular notification based on it's content. Click on the hamburger menu of each app **☰**, so the the required rules.

<img src=images/notifications/filter01.jpg width=200>
<img src=images/notifications/filter02.jpg width=200>
<img src=images/notifications/filter03.jpg width=200>

### Send notifications to device from other apps

It's possible to send notifications directly to the device using Intents, trough [PebbleKit](PebbleKit-Android-App-Compatibility) notifications. Despite having PebbleKit in the name, these work for any device.

1. In the notification settings page, set `Pebble Messages` to `When screen is off` or `Always`
2. Send a broadcast:

```bash
adb shell am broadcast \
    -a "com.getpebble.action.SEND_NOTIFICATION" \
    -e "messageType" "PEBBLE_ALERT" \
    -e "notificationData" '[{"title":"PebbleKitTest","body":"sent from Gadgetbridge"}]'
```
