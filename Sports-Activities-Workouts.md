---
gitea: none
include_toc: true
---

## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/basics/features/sports/


## Sports Activities

Some devices (MiBands, Amazfit...) can record "workouts" ("activities",
"trainings"...).  To create these recorded activities, you must start the
recording on the watch/band via the Training/Activity menu. These can then be
synchronized with Gadgetbridge. If you do not synchronize these activities for
a long time, the device might stop being able to record more data and will show
"Your data is full" message (or a similar one).

Use the running man icon <img src="images/workouts/icon_running.png"
alt="running man icon" width="20"> in the device card to open the Sports
Activities page:

<img src="images/workouts/device_menu.jpg" alt="device menu" width="200">

## Sports Activities page 

Here you can synchronize activities from the device, view them and also delete.

<img src="images/workouts/syncing.jpg" alt="activities list" width="200">


## Sports Activity data synchronization 

Syncing is always manual and it is not part of the automatic syncing. This
allows you to sync tour activities when you need (rather then when the app
decides), because it can take quite some time due to their size and the band is
not really usable during the sync.

Press the sync floating action button <img src="images/workouts/fab.png"
alt="sync button" width="20"> or swipe down. Please note that only one workout
will be fetched at a time, **you have to sync again to get the next activity
and so on**.

Gadgetbridge remembers the time of last synced item so if the workout data is
still in the band/watch (some devices erase it after transfer), you can
re-fetch the data anytime later by using the three dots menu ⋮ → `Reset fetch
time`, setting the time back (to date before this activity happened) and
re-syncing.

<img src="images/workouts/menu_reset.jpg" alt="menu reset" width="200">



## Selecting, management and viewing

Tapping on an Activity opens up a detail, long tap allows multiselection and
further Delete or Share the GPX file, if the activity contains it. You can also
share the GPX later from the Sports Activity Detail page.

<img src="images/workouts/selecting.jpg" alt="selecting" width="200">

## Filtering

You can use the Filter icon <img src="images/workouts/ic_filter.png" alt="icon
filter" width="20"> to only show listing of activities you like to see.

<img src="images/workouts/filter.jpg" alt="filter window" width="200">

## Summary statistics

You can see statistics calculation for the listed activities as a first item of
the list. You can also use the Filter icon <img
src="images/workouts/filter.png" alt="icon filter" width="20"> and get
statistics for the filtered activities only.

<img src="images/workouts/statistics.jpg" alt="statistics drawer" width="200">

## Sports Activity Detail

This shows details about the activity. If GPX is part of the activity, it can
be shared. There is a hidden function here: if you long tap on the activity
icon, the values will switch from recalculated (for example min/km) to raw, as
provided by the band (for example sec/m). This is useful for troubleshooting.

<img src="images/workouts/activity_detail.png" alt="activity detail"
width="200"> <img src="images/workouts/open_with.jpg" alt="open with"
width="200">

You can edit Name of the activity by tapping the pencil icon <img
src="images/workouts/ic_menu_edit.png" alt="pencil icon" width="20">.


<img src="images/workouts/label.jpg" alt="edit name" width="200">

## GPS data

### Bands without integrated GPS

[[Mi Band 3]], [[Mi Band 4]], [[Mi Band 5]], [[Amazfit Band 5]] and [[Mi Band 6]]
**with the official app** provide a feature to start an Activity which
combines GPS data (from the phone) with steps/heart rate data (from the band).

Mi Band 4 and above even allow you to start this activity from the band itself
and show GPS data on the band (current speed, distance). This is now supported
by Gadgetbridge, by enabling the "Send GPS during workout" setting and is
enabled for Mi Band 4, Mi Band 5, Amazfit Band 5 and Mi Band 6. 

### Bands with GPS

Some watches that [do have
GPS](https://codeberg.org/Freeyourgadget/Gadgetbridge/wiki/HUAMI-GPS) are for
example the [[Amazfit Bip]] and they do contain GPS track in the recorded
Sports activities.

### Integration with other tracking apps

If the band/watch does not have it's own GPS (and cannot do it in the way the
Mi Bands do as per above), the recorded Activity will not have GPS in the form
of GPX file, but you can use external sports tracking app and some dedicated
features of Gadgetbridge and the link the GPX to your Sports Activity, see
[[Integrating with Sports Tracking apps]].

## GPX files

Activity which contain GPS track can export this GPS data in the form of [GPX
file](https://www.topografix.com/gpx.asp).

GPX files can be found in in the `files`
[folder](Data-Export-Import-Merging-Processing#export-import-folder)
`android/data/nodomain.freyourgadget.gadgetbridge/files` along with log files.
The prefix is "gadgetbridge-track-" followed by the date/time.

If you like to make a GPS record of your activity, you can also use another
sports tracking app and utilize some dedicated Gadgetbridge features that make
integration nicer. This allows to utilize the Activity tracking on the watch
with some addition details and processing, like number of steps or strokes and
so on.

This now offers a new possibility to link a GPX file to a sports activity that
did not even allow to have a GPS recording in the first place - like outdoor
rowing while using the rowing machine recording or pool swimming (if you have
a waterproof phone).

Some activities that you can record without GPS (and make some sense to have
GPS linked) include Workout, Outdoor running, Outdoor walking, Treadmill,
Freestyle, Indoor cycling, Rowing machine, Pool swimming. These of course must
exist in the band/watch, which depends on the watch version.

To link a GPX to an activity, you must either manually copy the GPX file to the
`files` folder or you can use the Share icon in the sports tracking app and use
the Gadgetbridge GPX Receiver, which will copy the file to the `files` folder.

Sharing a file:

<img src=images/integrating_sports/06receiver.jpg width=220 alt="settings screenshot">

This will open Gadgetbridge GPX Receiver, confirm OK.

<img src=images/integrating_sports/14ok.jpg width=220 alt="settings screenshot">

Existing files overwriting is also indicated:

<img src=images/integrating_sports/15overwrite.jpg width=220 alt="settings screenshot">


### Link recorded GPX with the Gadgetbridge Sports Activity

In Gadgetbridge, open recorded Sports Activity

<img src=images/integrating_sports/01workouts.jpg width=220 alt="settings screenshot">

Edit GPX track

<img src=images/integrating_sports/13editgps.jpg width=220 alt="settings screenshot">

Select the track

<img src=images/integrating_sports/13_select.jpg width=220 alt="settings screenshot">

Confirm

<img src=images/integrating_sports/16ok.jpg width=220 alt="settings screenshot">

GPS track is now added

<img src=images/integrating_sports/17.jpg width=220 alt="settings screenshot">

See a [dedicated
post](https://blog.freeyourgadget.org/add_gps_track_to_any_sports_activity.html)
on our blog and see also a full manual of [Integrating Sports Tracking apps
with Gadgetbridge Sports
Activities/Workouts](Integrating-with-Sports-Tracking-apps) on how to set this
up.


## View GPX data

You can view the GPX file with for example these FLOSS applications:

- on mobile:
    - [OpenTracks](https://f-droid.org/packages/de.dennisguse.opentracks)
    - [OSM Dashboard for OpenTracks](https://f-droid.org/packages/de.storchp.opentracks.osmplugin/)
    - [OSM Dashboard Offline for OpenTracks](https://f-droid.org/packages/de.storchp.opentracks.osmplugin.offline/)
    - [FitoTrack ](https://f-droid.org/packages/de.tadris.fitness) (not via
      sharing icon, but by using import function in FitoTrack)
    - [Another Android Tracker](https://f-droid.org/packages/ch.bailu.aat/)
      (seems like more precise representation of time and distance, calculates
      calories)
    - [osmAnd+](https://f-droid.org/packages/net.osmand.plus/) 
    - [Milkha](https://apt.izzysoft.de/fdroid/index/apk/com.tensorfoo.milkha) 

- on desktop:
   - [GPXSee](https://www.gpxsee.org/) (shows also heart rate if included in the file)


## Not supported features

Please be aware that not every detail of a given workout is currently being
displayed in Gadgetbridge but the full workout is retrieved and stored. So as
interpreting improves, the displayed data improves too. Interpreting the data
is a challenge, if you spot an issue, try to figure out what the correct
representation might be and please report.

## Other related articles

- [Integrating Sports Tracking apps with Gadgetbridge Sports Activities/Workouts](Integrating-with-Sports-Tracking-apps)

