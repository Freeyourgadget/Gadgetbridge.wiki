---
gitea: none
include_toc: true
---

## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/basics/features/sports/#heart-rate

## Heartrate measurement on MiBand/Amazfit

### Whole day HR measurement

You can set an interval of minimal HR checking:

- OFF
- Every minute
- Every 5 minutes
- Every 10 minutes
- Every 30 minutes
- Once an hour

And the band will take measurements at least once in this interval. If an
activity is detected, more frequent measurements are taken, capturing the
entire activity in detail. So it is often sufficient to use the `Once an hour`
interval and the band will capture all your active time, plus one sample per
hour during idle time.


### Sleep HR measurement

The Use heartrate sensor to improve sleep detection enables HR measurement when
sleep was detected by the watch. Whole day measurement (as per above) must be
enabled for this feature to work.

### Bluetooth Heart rate sensor sharing to another apps

Some devices (probably all or most Huami bands like [[Mi Band 2]], [[Mi Band
3]], [[Mi Band 4]], [[Mi Band 5]], [[Amazfit Bip]] and more) support acting as
a BLE HR sensor. You can enable this feature in the Device specific settings:
_3rd party realtime HR access_. Do note, that some devices seem to have this
actually ON all the time and it cannot be disabled (Amazfit Bip, Amazfit Cor2,
AmazfitBipLite, MiBand2). 

There is another settings, to actually make the device visible: _Visible while
connected_, make sure this is also enabled, so the below mentioned apps can
actually find the sensor over bluetooth.

<img src=images/integrating_sports/02.jpg height=200 alt="settings screenshot">

Once enabled (_Visible while connected_ and _3rd party realtime HR access_) ,
Gadgetbridge doesn't even need to be running and the data is still available
via Bluetooth LE. For the Heart rate data to be actually measured and sent you
need to either run an Activity on the band/watch or be in the Heartrate/Status
screen (few seconds value only), or have [Whole day HR
measurement](#whole-day-hr-measurement) enabled. Because the whole day
measurement detects activity automatically, once you get moving, HR
measurements are taken frequently.

Some example known apps to work with these data:

- [OpenTracks](https://f-droid.org/packages/de.dennisguse.opentracks)
- [FitoTrack ](https://f-droid.org/packages/de.tadris.fitness)
- [BLE Explorer](https://f-droid.org/packages/org.ligi.blexplorer)
- [BLE
  Monitor](https://android.izzysoft.de/repo/apk/com.huc.android_ble_monitor)
- [nRF Toolbox](https://github.com/NordicSemiconductor/Android-nRF-Toolbox)
  ([on Play
  Store](https://play.google.com/store/apps/details?id=no.nordicsemi.android.nrftoolbox&hl=en&gl=US))

**See full manual of [Integrating Sports Tracking apps with Gadgetbridge Sports Activities/Workouts](Integrating-with-Sports-Tracking-apps) on how to set this up.**

### Here is how you can see your HR in BLE Explorer

Once the sharing is enabled and activity is started on the device (or status
screen on Bip is used) live data is visible in BLE Explorer, in the Heartrate
section, first line (the 00002a37... characteristics). You must enable the
notify toggle. GB does not need to run nor be connected anymore, the sensor
remains available but you must keep the activity running, for fresh data to be
sent.
