## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/gadgets/wearables/amazfit/#gts

Contents
* [[Pairing]]
* [Alarms](Huami-Alarms)
* [Gadgetbridge Configuration](Configuration#mi-band--amazfit-specific-settings)
* [Sports Activities - Workouts](Sports-Activities-Workouts)

Please see wiki pages for other Amazfit devices (mainly [Amazfit Bip](Amazfit-Bip)) and link relevant articles here if they apply to this device, much appreciated!
