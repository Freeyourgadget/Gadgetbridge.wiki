## This page has moved

⚠<fe0f> The wiki has been replaced by the new website - this page has been moved: https://gadgetbridge.org/internals/automations/intents/

# Bluetooth Intent API

This page has been superseded by [Intent API](Intent-API)
